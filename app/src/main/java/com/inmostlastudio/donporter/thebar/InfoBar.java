package com.inmostlastudio.donporter.thebar;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.inmostlastudio.donporter.barsInfo.TheBars;
import com.inmostlastudio.donporter.contact.DonPorter;
import com.inmostlastudio.donporter.settings.SettingsActivity;
import com.inmostlastudio.donporter.thebar.gallery.GalleryBar;
import com.ms.square.android.expandabletextview.ExpandableTextView;
import com.parse.ParseAnalytics;
import com.squareup.picasso.Picasso;

import com.inmostlastudio.donporter.R;

import java.util.HashMap;
import java.util.Map;

/*
    Copyright 2014 Manabu Shimobe

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
 */

/**
 * Created by IN MOSTLA Studio on 04/11/2015.
 *
 * Muestra el contacto completo del bar.
 * Fab para elección de favoritos y wishlist.
 * Botón para direccionamiento de la ubicación del bar con Maps.
 */

public class InfoBar extends AppCompatActivity {

    //Variables
    String snackMessage;

    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    Point size;
    Toolbar toolbar;
    AppBarLayout appBarLayout;

    static String barName;
    String parseClass;
    String motto, description, address, contact, location;
    String userFacebookName, userFacebookId, userTwitter, userInstagram;
    //String colorBackground, colorText, colorName, colorLabel;
    String colorFab = "#006B33";
                        //"006B33" Verde
                        //"ff8d00" Naranja
                        //"3F51B5" Azul
    String pref_fav, pref_wish;
    boolean fav, wish;

    int idLogo, idImage;
    TextView barMotto, barLocation, barContact;
    ImageView barLogo, barImage, galleryLogo, facebook, twitter, instagram;
    FloatingActionButton fab;
    Button mapsButton;

    static final String BAR_NAME = "name";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bar_info);

        preferences = getSharedPreferences("FAV_WISH_FILE", Context.MODE_PRIVATE);
        if( getIntent().getExtras() != null ) {
            Bundle extras = getIntent().getExtras();
            barName = extras.getString("NAME");
            //barName = "IN MOSTLA Studio"; //
        }

        getBar();
        settingToolBar();
        layoutSize();

        appBarLayout = (AppBarLayout) findViewById(R.id.appbar_home_bar);
        appBarLayout.getLayoutParams().height = size.y/2;

        CollapsingToolbarLayout collapsingToolbar = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbar.setTitle(barName);

        barLogo = (ImageView) findViewById(R.id.header_logo);
        Picasso.with(this).load(idLogo).into(barLogo);

        barImage = (ImageView) findViewById(R.id.header);
        Picasso.with(this).load(idImage).into(barImage);

        galleryLogo = (ImageView) findViewById(R.id.gallery_logo);
        Picasso.with(this).load(idImage).into(galleryLogo);
        galleryLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent gllry = new Intent(InfoBar.this, GalleryBar.class);
                gllry.putExtra("NAME", barName);
                gllry.putExtra("PARSE_CLASS", parseClass);
                startActivity(gllry);
            }
        });

        View view = findViewById(R.id.nested_view);

        barMotto = (TextView) view.findViewById(R.id.bar_motto);
        barMotto.setText(motto);

        ExpandableTextView expandableTextView = (ExpandableTextView) view.findViewById(R.id.expand_text_view);
        expandableTextView.setText(description);

        barLocation = (TextView) view.findViewById(R.id.bar_location);
        barLocation.setText(address);
        barContact = (TextView) view.findViewById(R.id.bar_contact);
        barContact.setText(contact);

        mapsButton = (Button) view.findViewById(R.id.maps_button);
        mapsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String coordenadas = "geo:0,0?q= " + location;

                Intent maps = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(coordenadas));
                // Try/Catch en el caso que no tenga Maps App instalada, abre browser.
                try {
                    startActivity(maps);
                } catch (ActivityNotFoundException ex) {
                    try {
                        Intent unrestrictedIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(coordenadas));
                        startActivity(unrestrictedIntent);
                    } catch (ActivityNotFoundException innerEx) {
                        Toast.makeText(InfoBar.this, "Por favor instala la aplicación de Google Maps",
                                Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

        fab = (FloatingActionButton) findViewById(R.id.fab_bar_home);
        fab.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(colorFab)));
        if(fav) {
            fab.setImageResource(R.drawable.ic_fav_star_check);
        }else if(wish) {
            fab.setImageResource(R.drawable.ic_wishlist_check);
        }
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(fav) {
                    fav = false;
                    wish = true;
                    fab.setImageResource(R.drawable.ic_wishlist_check);
                    snackMessage = "Agregado a la lista de deseos";
                }else if(wish) {
                    fav = false;
                    wish = false;
                    fab.setImageResource(R.drawable.ic_action_add);
                    snackMessage = "Removido de la lista de deseos";
                } else {
                    fav = true;
                    wish = false;
                    fab.setImageResource(R.drawable.ic_fav_star_check);
                    snackMessage = "Agregado a tus favoritos";
                }

                Snackbar.make(findViewById(R.id.coordinator), snackMessage, Snackbar.LENGTH_LONG)
                        .setAction("undo", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                    if(fav) {
                        fav = false;
                        wish = false;
                        fab.setImageResource(R.drawable.ic_action_add);
                    }else if(wish) {
                        fav = true;
                        wish = false;
                        fab.setImageResource(R.drawable.ic_fav_star_check);
                    } else {
                        fav = false;
                        wish = true;
                        fab.setImageResource(R.drawable.ic_wishlist_check);
                    }
                    }
                }).show();

                editor = preferences.edit();
                editor.putBoolean(pref_fav, fav);
                editor.putBoolean(pref_wish, wish);
                editor.apply();
            }
        });

        facebook = (ImageView) view.findViewById(R.id.icon_social_facebook);
        facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if( !userFacebookName.equals("") && !userFacebookId.equals("")) {
                    startActivity(getOpenFacebookIntent(InfoBar.this));
                } else {
                    Toast.makeText(InfoBar.this, "No cuenta con página en Facebook", Toast.LENGTH_LONG).show();
                }
            }
        });
        twitter = (ImageView) view.findViewById(R.id.icon_social_twitter);
        twitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if( !userTwitter.equals(""))
                    startActivity(getOpenTwitterIntent(InfoBar.this));
                else
                    Toast.makeText(InfoBar.this, "No posee cuenta en Twitter", Toast.LENGTH_LONG).show();
            }
        });
        instagram = (ImageView) view.findViewById(R.id.icon_social_instagram);
        instagram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if( !userInstagram.equals(""))
                    startActivity(getOpenInstagramIntent(InfoBar.this));
                else
                    Toast.makeText(InfoBar.this, "No posee cuenta en Instagram", Toast.LENGTH_LONG).show();

            }
        });

        Map<String, String> infoAnalytics = new HashMap<>();
        infoAnalytics.put(barName, "info");
        ParseAnalytics.trackEventInBackground(parseClass,infoAnalytics);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putString(BAR_NAME, barName);

        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        // Always call the superclass so it can restore the view hierarchy
        super.onRestoreInstanceState(savedInstanceState);

        barName = savedInstanceState.getString(BAR_NAME);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_default, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        //noinspection SimplifiableIfStatement
        switch (item.getItemId()) {
            case R.id.action_donporter:
                Intent who = new Intent(this, DonPorter.class);
                startActivity(who);
                break;
            case R.id.action_settings:
                Intent settings = new Intent(this, SettingsActivity.class);
                startActivity(settings);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    public void settingToolBar() {
        toolbar = (Toolbar) findViewById(R.id.anim_toolbar);
        toolbar.setTitle(barName);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);

        // Si se encuentra dentro del rango de la versión.
        toolbar.setPadding(0, 0, 0, 0);
        setSupportActionBar(toolbar);
    }

    public void getBar() {
        TheBars.setBar(barName);

        motto = TheBars.getCardView_motto();
        description = TheBars.getLongDescription();
        address = TheBars.getAddress();
        contact = TheBars.getContact();
        location = TheBars.getMaps_location();
        pref_fav = TheBars.getPref_Fav();
        pref_wish = TheBars.getPref_Wish();
        idLogo = TheBars.getLogoId();
        idImage = TheBars.getImageId();

        userFacebookName = TheBars.getSocialFacebookName();
        userFacebookId = TheBars.getSocialFacebookPageId();
        userTwitter = TheBars.getSocialTwitter();
        userInstagram = TheBars.getSocialInstagram();

        parseClass = TheBars.getParseClass();

        fav = preferences.getBoolean(pref_fav, false);
        wish = preferences.getBoolean(pref_wish, false);
    }

    // Get layout size
    public void layoutSize(){
        Display display = getWindowManager().getDefaultDisplay();
        size = new Point();
        display.getSize(size);
    }

    public Intent getOpenFacebookIntent(Context context) {
        try {
            context.getPackageManager().getPackageInfo("com.facebook.katana", 0);
            return new Intent(Intent.ACTION_VIEW, Uri.parse("fb://page/" + userFacebookId));
        } catch (Exception e) {
            return new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://www.facebook.com/" + userFacebookName));
        }
    }

    public Intent getOpenTwitterIntent(Context context) {
        try {
            context.getPackageManager().getPackageInfo("com.twitter.android", 0);
            return new Intent(Intent.ACTION_VIEW, Uri.parse("twitter://user?screen_name=" + userTwitter));
        } catch (Exception e) {
            return new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://twitter.com/" + userTwitter));
        }
    }

    public Intent getOpenInstagramIntent(Context context) {
        try {
            context.getPackageManager().getPackageInfo("com.instagram.android",0);
            return new Intent(Intent.ACTION_VIEW, Uri.parse("http://instagram.com/_u/" + userInstagram));
        } catch (Exception e) {
            return new Intent(Intent.ACTION_VIEW, Uri.parse("http://instagram.com/" + userInstagram));
        }
    }

}
