package com.inmostlastudio.donporter.thebar.promotions;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

import com.inmostlastudio.donporter.R;
import com.inmostlastudio.donporter.barsInfo.TheBars;
import com.inmostlastudio.donporter.contact.DonPorter;
import com.inmostlastudio.donporter.settings.SettingsActivity;
import com.inmostlastudio.donporter.utils.Utils;
import com.parse.ParseAnalytics;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by IN MOSTLA Studio on 14/10/2015.
 *
 * Muestra las promociones del bar, especificadas desde una base de datos en Parse.
 */

// TODO: 14/10/2015 Vista de galeria. Zoom en imágenes

public class PromosEventsBar extends AppCompatActivity {

    int toolbarHeight, statusbarHeight, height;
    String barName, parseClass, type;
    Point size;
    Toolbar toolbar;

    ProgressDialog mProgressDialog;
    private List<PromosEventsCard> list;
    List<ParseObject> objList;
    ListView view;
    PromosEventsList adapterList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bar_promosevents);
        if( getIntent().getExtras() != null ) {
            Bundle extras = getIntent().getExtras();
            barName = extras.getString("NAME");
            type = extras.getString("EVENTS_PROMOS");
        }

        TheBars.setBar(barName);
        settingToolBar();
        setParseClass();
        layoutSize();

        // Set the padding to match the Status Bar height
        toolbarHeight = Utils.getToolbarHeight(this);
        statusbarHeight = Utils.getStatusBarHeight(this);
        if(Build.VERSION.SDK_INT >= 21 || Build.VERSION.SDK_INT <= 17 ) {
            toolbar.setPadding(0, 0, 0, 0);
            height = toolbarHeight;
        }
        else {
            toolbar.setPadding(0, statusbarHeight, 0, 0);
            height = toolbarHeight + statusbarHeight;
        }

        view = (ListView) findViewById(R.id.promotions_list);

        new RemoteDataTask().execute();

        Map<String, String> promosEventsAnalytics = new HashMap<>();
        if(type.equals("promos")) {
            promosEventsAnalytics.put(barName, "promos");
        } else {
            promosEventsAnalytics.put(barName, "events");
        }
        ParseAnalytics.trackEventInBackground(parseClass, promosEventsAnalytics);
    }

    @Override
    public void onResume() {
        super.onResume();
        /** Verifica el estado de conexiÃ³n del dispositivo */
        if (!Utils.verificaConexion(this)) {
            Toast.makeText(getBaseContext(),
                    "No cuentas con conexión a Internet", Toast.LENGTH_LONG)
                    .show();
        }
    }

    private class RemoteDataTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog = new ProgressDialog(PromosEventsBar.this);
            if(type.equals("promos")) {
                mProgressDialog.setTitle("Promociones");
            } else if(type.equals("events")) {
                mProgressDialog.setTitle("Eventos");
            }
            // Set progressdialog message
            mProgressDialog.setMessage("Cargando...");
            mProgressDialog.setIndeterminate(false);
            // Show progressdialog
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            list = new ArrayList<>();
            try {
                ParseQuery<ParseObject> query = new ParseQuery<>(parseClass);
                query.whereEqualTo("type", type);
                query.orderByAscending("weight");
                objList = query.find();
                for (ParseObject item : objList) {
                        PromosEventsCard promosEventsCard = new PromosEventsCard();

                        promosEventsCard.setTitle(item.getString("item"));
                        ParseFile image = item.getParseFile("image");
                        promosEventsCard.setImage(image.getUrl());
                        list.add(promosEventsCard);
                }

            } catch (ParseException e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            adapterList = new PromosEventsList(PromosEventsBar.this, list, type, size);
            view.setAdapter(adapterList);
            mProgressDialog.dismiss();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_default, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_donporter:
                Intent who = new Intent(this, DonPorter.class);
                startActivity(who);
                break;
            case R.id.action_settings:
                Intent settings = new Intent(this, SettingsActivity.class);
                startActivity(settings);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void settingToolBar() {
        toolbar = (Toolbar) findViewById(R.id.appbar_promos);
        toolbar.setTitle(barName);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        setSupportActionBar(toolbar);
    }

    //Get layout size
    public void layoutSize() {
        Display display = getWindowManager().getDefaultDisplay();

        size = new Point();
        display.getSize(size);
    }

    /* Establece la clase de Parse, de acuerdo al bar seleccionado */
    public void setParseClass() {
        parseClass = TheBars.getParseClass();
    }
}
