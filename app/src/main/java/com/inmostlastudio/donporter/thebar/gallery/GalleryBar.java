package com.inmostlastudio.donporter.thebar.gallery;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

import com.inmostlastudio.donporter.R;
import com.inmostlastudio.donporter.contact.DonPorter;
import com.inmostlastudio.donporter.settings.SettingsActivity;
import com.inmostlastudio.donporter.utils.Utils;
import com.parse.ParseAnalytics;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by IN MOSTLA Studio on 21/10/2015.
 *
 * Muestra una serie de fotos almacenadas desde Parse.
 */

// TODO DESIGN: 21/10/2015 Gallery. tipo de galería

public class GalleryBar extends AppCompatActivity {

    String barName, parseClass;
    Toolbar toolbar;

    private List<String> list;
    List<ParseObject> objList;
    ProgressDialog mProgressDialog;

    ListView view;
    GalleryList adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bar_galley);
        if (getIntent().getExtras() != null) {
            Bundle extras = getIntent().getExtras();
            barName = extras.getString("NAME");
            parseClass = extras.getString("PARSE_CLASS");
        }
        settingToolBar();

        new RemoteData().execute();
        Map<String, String> galleryAnalytics = new HashMap<>();
        galleryAnalytics.put(barName, "gallery");
        ParseAnalytics.trackEventInBackground(parseClass, galleryAnalytics);

    }

    @Override
    public void onResume() {
        super.onResume();
        /** Verifica el estado de conexiÃ³n del dispositivo */
        if (!Utils.verificaConexion(this)) {
            Toast.makeText(getBaseContext(),
                    "No cuentas con conexión a Internet", Toast.LENGTH_LONG)
                    .show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_default, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_donporter:
                Intent who = new Intent(this, DonPorter.class);
                startActivity(who);
                break;
            case R.id.action_settings:
                Intent settings = new Intent(this, SettingsActivity.class);
                startActivity(settings);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void settingToolBar() {
        toolbar = (Toolbar) findViewById(R.id.appbar_gallery);
        toolbar.setTitle(barName);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        setSupportActionBar(toolbar);
    }

    private class RemoteData extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog = new ProgressDialog(GalleryBar.this);
            mProgressDialog.setTitle("Galería");
            // Set progressdialog message
            mProgressDialog.setMessage("Cargando...");
            mProgressDialog.setIndeterminate(false);
            // Show progressdialog
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            list = new ArrayList<>();
            try {
                ParseQuery<ParseObject> query = new ParseQuery<>(parseClass);
                query.whereEqualTo("type","gallery");
                query.orderByAscending("weight");
                objList = query.find();
                for(ParseObject item : objList) {
                        ParseFile image = item.getParseFile("image");
                        list.add(image.getUrl());
                }

            } catch (ParseException e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            view = (ListView) findViewById(R.id.gallery_list);

            adapter = new GalleryList(GalleryBar.this, list);
            view.setAdapter(adapter);
            mProgressDialog.dismiss();
        }

    }

}
