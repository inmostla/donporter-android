package com.inmostlastudio.donporter.thebar.promotions;

import android.content.Context;
import android.graphics.Point;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.inmostlastudio.donporter.R;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by IN MOSTLA Studio on 17/11/2015.
 *
 */

public class PromosEventsList extends BaseAdapter {

    Point screen;
    String type;
    Context mContext;
    LayoutInflater inflater;
    List<PromosEventsCard> promosList;

    public PromosEventsList(Context context, List<PromosEventsCard> promos, String type, Point size) {
        mContext = context;
        this.type = type;
        promosList = promos;
        screen = size;
        inflater = LayoutInflater.from(context);
    }

    public class ViewHolder {
        TextView title;
        ImageView image;
    }

    @Override
    public int getCount() {
        return promosList.size();
    }

    @Override
    public Object getItem(int position) {
        return promosList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;

        if(convertView == null) {
            holder = new ViewHolder();
            inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView = inflater.inflate(R.layout.promosevents_item, null);

            holder.title = (TextView) convertView.findViewById(R.id.promotions_title);
            holder.image = (ImageView) convertView.findViewById(R.id.promotions_image);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.title.setText(promosList.get(position).getTitle());

        // TODO DESIGN: 24/12/2015 Establecer el cálculo del tamaño de acuerdo a la pantalla
        if( type.equals("promos")) {
            Picasso.with(mContext).load(promosList.get(position).getImage()).resize(screen.x, screen.y / 3)
                    .into(holder.image);
        } else { /* Events */
            Picasso.with(mContext).load(promosList.get(position).getImage()).resize(screen.x, screen.y - screen.y / 4)
                    .into(holder.image);
        }

        return convertView;
    }


}
