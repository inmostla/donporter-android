package com.inmostlastudio.donporter.thebar.menu;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

import com.inmostlastudio.donporter.R;
import com.inmostlastudio.donporter.barsInfo.TheBars;
import com.inmostlastudio.donporter.contact.DonPorter;
import com.inmostlastudio.donporter.settings.SettingsActivity;
import com.inmostlastudio.donporter.utils.Utils;

import com.parse.ParseAnalytics;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by IN MOSTLA Studio on 14/10/2015.
 *
 * Actividad para obtener los datos del menú desde Parse.
 * Se muestran por medio de RecyclerViews seccionados.
 *
 */

// TODO DESIGN: 21/01/2016 Mejorar la vista del menú, tipo menú físico.

public class MenuBar extends AppCompatActivity {

    // Variables
    private static String barName, parseClass;
    int toolbarHeight, statusbarHeight, height;
    Toolbar toolbar;

    List<ParseObject> objList;
    ProgressDialog mProgressDialog;
    private List<MenuCard> menuList = null;

    // ListView option
    ListView listView;
    MenuSectionedList menuListViewAdapter;

    /* Constructor */
        public MenuBar() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bar_menu);

        // Obtiene el nombre del bar seleccionado
        if( getIntent().getExtras() != null ) {
            Bundle extras = getIntent().getExtras();
            barName = extras.getString("NAME");
        }

        // Inicia el toolbar
        settingToolBar();
        // Estable la clase de Parse dependiendo del bar
        setParseClass();

        // Agrega el menú para la aplicación ejemplo
        // setMenuItemsExample();

        // Set the padding to match the Status Bar height
        toolbarHeight = Utils.getToolbarHeight(this);
        statusbarHeight = Utils.getStatusBarHeight(this);
        if(Build.VERSION.SDK_INT >= 21 || Build.VERSION.SDK_INT <= 17 ) {
            toolbar.setPadding(0, 0, 0, 0);
            height = toolbarHeight;
        }
        else {
            toolbar.setPadding(0, statusbarHeight, 0, 0);
            height = toolbarHeight + statusbarHeight;
        }

        //Ejecuta el AsyncTask
        new RemoteData().execute();

        Map<String, String> menuAnalytics = new HashMap<>();
        menuAnalytics.put(barName, "menu");
        ParseAnalytics.trackEventInBackground(parseClass, menuAnalytics);

    }

    @Override
    public void onResume() {
        super.onResume();
        /** Verifica el estado de conexiÃ³n del dispositivo */
        if (!Utils.verificaConexion(this)) {
            Toast.makeText(getBaseContext(),
                    "No cuentas con conexión a Internet", Toast.LENGTH_LONG)
                    .show();
        }
    }

    private class RemoteData extends AsyncTask< Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //Create progress dialog
            mProgressDialog = new ProgressDialog(MenuBar.this);
            //Set progress dialog title
            mProgressDialog.setTitle("Menú");
            //Set progress dialog message
            mProgressDialog.setMessage("Cargando...");
            mProgressDialog.setIndeterminate(false);
            //Show progress dialog
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            //Create array
            menuList = new ArrayList<>();
            try {
                //Localizar la tabla en Parse
                ParseQuery<ParseObject> query = new ParseQuery<>(parseClass);
                query.whereEqualTo("type", "menu");
                query.orderByAscending("weight");
                objList = query.find();
                String aux = "";

                for(ParseObject item : objList) {
                        MenuCard menuCard = new MenuCard();

                        if (menuList.size() == 0) {
                            MenuCard menuCardHeader = new MenuCard();
                            menuCardHeader.setHeader(true);
                            menuCardHeader.setItem(item.getString("section"));
                            menuList.add(menuCardHeader);
                        } else if (!item.get("section").equals(aux)) {
                            MenuCard menuCardHeader = new MenuCard();
                            menuCardHeader.setHeader(true);
                            menuCardHeader.setItem(item.getString("section"));
                            menuList.add(menuCardHeader);
                        }

                        menuCard.setHeader(false);
                        menuCard.setItem( item.getString("item"));
                        menuCard.setDescription(item.getString("description"));
                        menuCard.setPrice( item.getInt("price"));
                        menuList.add(menuCard);

                        aux = item.getString("section");
                }

            } catch (ParseException e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            //ListView. Busca el ListView en el layout
            listView = (ListView) findViewById(R.id.menu_listview);
            //ListView. Crea un nuevo adaptador con la lista de elementos
            menuListViewAdapter = new MenuSectionedList(MenuBar.this, menuList);
            //ListView. Agrega el adaptador
            listView.setAdapter(menuListViewAdapter);

            //Termina el ProgressDialog
            mProgressDialog.dismiss();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_default, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_donporter:
                Intent who = new Intent(this, DonPorter.class);
                startActivity(who);
                break;
            case R.id.action_settings:
                Intent settings = new Intent(this, SettingsActivity.class);
                startActivity(settings);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    /* Inicializa el Toolbar */
    public void settingToolBar() {
        toolbar = (Toolbar) findViewById(R.id.appbar_menu);
        toolbar.setTitle(barName);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        setSupportActionBar(toolbar);
    }

    /* Establece la clase de Parse, de acuerdo al bar seleccionado */
    public void setParseClass() {
        parseClass = TheBars.getParseClass();
    }

}
