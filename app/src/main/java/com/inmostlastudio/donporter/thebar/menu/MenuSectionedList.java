package com.inmostlastudio.donporter.thebar.menu;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.inmostlastudio.donporter.R;

import java.util.List;

/**
 * Created by IN MOSTLA Studio on 25/11/2015.
 *
 * Adaptador de ListView para la lista del menú.
 *
 */

public class MenuSectionedList extends BaseAdapter {

    //Variables
    String money = "$";
    Context mContext;
    LayoutInflater inflater;
    private List<MenuCard> menuList = null;

    // View Type for Separators
    private static final int ITEM_VIEW_TYPE_SEPARATOR = 0;
    // View Type for Regular rows
    private static final int ITEM_VIEW_TYPE_REGULAR = 1;
    // -- Separators and Regular rows --
    private static final int ITEM_VIEW_TYPE_COUNT = 2;

    public MenuSectionedList (Context context, List<MenuCard> menu) {
        mContext = context;
        menuList = menu;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return menuList.size();
    }

    @Override
    public Object getItem(int position) {
        return menuList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return ITEM_VIEW_TYPE_COUNT;
    }

    @Override
    public int getItemViewType(int position) {
        boolean isSection = menuList.get(position).isHeader();

        if (isSection) {
            return ITEM_VIEW_TYPE_SEPARATOR;
        }
        else {
            return ITEM_VIEW_TYPE_REGULAR;
        }
    }

    @Override
    public boolean isEnabled(int position) {
        return getItemViewType(position) != ITEM_VIEW_TYPE_SEPARATOR;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        View view;
        int itemViewType = getItemViewType(position);

        if(convertView == null) {
            inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            if (itemViewType == ITEM_VIEW_TYPE_SEPARATOR) {     //header
                view = inflater.inflate(R.layout.menu_section, null);
            } else {    //item
                view = inflater.inflate(R.layout.menu_item, null);
            }
        } else {
            view = convertView;
        }

        if (itemViewType == ITEM_VIEW_TYPE_SEPARATOR) {     //header
            TextView section = (TextView) view.findViewById(R.id.menu_section_text);
            section.setText(menuList.get(position).getItem());
        } else {    //item
            TextView item = (TextView) view.findViewById(R.id.menu_title);
            TextView price = (TextView) view.findViewById(R.id.menu_price);
            TextView description = (TextView) view.findViewById(R.id.menu_description);

            item.setText(menuList.get(position).getItem());
            //Convierte int a String
            money = "$" + String.valueOf(menuList.get(position).getPrice());
            price.setText(money);
            description.setText(menuList.get(position).getDescription());
        }

        //Elimina la opción de hacer click en cada elemento
        view.setOnClickListener(null);
        return view;
    }
}
