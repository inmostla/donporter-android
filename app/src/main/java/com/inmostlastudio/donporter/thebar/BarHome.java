package com.inmostlastudio.donporter.thebar;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Point;
import android.graphics.drawable.AnimationDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
//import android.os.Looper;
//import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.GridLayout;
import android.widget.ImageView;

import com.inmostlastudio.donporter.R;
import com.inmostlastudio.donporter.barsInfo.TheBars;
import com.inmostlastudio.donporter.contact.DonPorter;
import com.inmostlastudio.donporter.settings.SettingsActivity;
import com.inmostlastudio.donporter.thebar.menu.MenuBar;
import com.inmostlastudio.donporter.thebar.promotions.PromosEventsBar;
import com.inmostlastudio.donporter.utils.Utils;

/**
 * Created by IN MOSTLA Studio on 13/09/2015.
 *
 * First view after clicking the bar.
 * Show 4 buttons to entry TheBar, Menu, Promotions or Events.
 */

// TODO DESIGN: 14/10/2015 transición por el click del botón
// TODO: 19/01/2016 Realizar los Handlers apropiados para onStop en animaciones

public class BarHome extends AppCompatActivity {
    SharedPreferences preferences;

    static String barName;

    int toolbarHeight, statusbarHeight, height;
    int buttonsSize;
    Point size;
    Toolbar toolbar;
    GridLayout buttonsGrid;
    ImageView imageBar, imageMenu, imagePromos, imageEvents, bottomDonPorter;
    AnimationDrawable animationBar, animationMenu, animationEvents, animationPromos;

    Handler animationHandler;
//    Thread thread;

    static final String BAR_NAME = "name";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bar_home);

        if( getIntent().getExtras() != null ) {
            Bundle extras = getIntent().getExtras();
            barName = extras.getString("NAME");
        }

        TheBars.setBar(barName);
        preferences = this.getSharedPreferences("FAV_WISH_FILE", Context.MODE_PRIVATE);
        settingToolBar();
        // Set the padding to match the Status Bar height
        toolbarHeight = Utils.getToolbarHeight(this);
        statusbarHeight = Utils.getStatusBarHeight(this);
        if(Build.VERSION.SDK_INT >= 21 || Build.VERSION.SDK_INT <= 17 ) {
            toolbar.setPadding(0, 0, 0, 0);
            height = toolbarHeight;
        }
        else {
            toolbar.setPadding(0, statusbarHeight, 0, 0);
            height = toolbarHeight + statusbarHeight;
        }
        buttonsGrid = (GridLayout) findViewById(R.id.buttons_grid);

        layoutSize();
        animationHandler = new Handler();
//        thread = new Thread();

        imageBar = (ImageView) findViewById(R.id.anim_button_bar);
        imageBar.getLayoutParams().height = buttonsSize;
        imageBar.getLayoutParams().width = buttonsSize;
        imageBar.setBackgroundResource(R.drawable.animation_bar_info);
        animationBar = (AnimationDrawable) imageBar.getBackground();

        imageBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                animationBar.stop();
                animationBar.start();

                animationHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent home = new Intent(getBaseContext(), InfoBar.class);
                        home.putExtra("NAME", barName);
                        startActivity(home);
                    }
                }, getTotalDuration(animationBar));
            }
        });

        imageMenu = (ImageView) findViewById(R.id.anim_button_menu);
        imageMenu.getLayoutParams().height = buttonsSize;
        imageMenu.getLayoutParams().width = buttonsSize;
        imageMenu.setBackgroundResource(R.drawable.animation_menu);
        animationMenu = (AnimationDrawable) imageMenu.getBackground();
        imageMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                animationMenu.stop();
                animationMenu.start();

                animationHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent menu = new Intent(getBaseContext(), MenuBar.class);
                        menu.putExtra("NAME", barName);
                        startActivity(menu);
                    }
                }, getTotalDuration(animationMenu));
            }
        });

        imagePromos = (ImageView) findViewById(R.id.anim_button_promos);
        imagePromos.getLayoutParams().height = buttonsSize;
        imagePromos.getLayoutParams().width = buttonsSize;
        imagePromos.setBackgroundResource(R.drawable.animation_promos);
        animationPromos = (AnimationDrawable) imagePromos.getBackground();
        imagePromos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                animationPromos.stop();
                animationPromos.start();

                animationHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent promos = new Intent(getBaseContext(), PromosEventsBar.class);
                        promos.putExtra("NAME", barName);
                        promos.putExtra("EVENTS_PROMOS", "promos");
                        startActivity(promos);
                    }
                }, getTotalDuration(animationPromos));

            }
        });

        imageEvents = (ImageView) findViewById(R.id.anim_button_event);
        imageEvents.getLayoutParams().width = buttonsSize;
        imageEvents.getLayoutParams().height = buttonsSize;
        imageEvents.setBackgroundResource(R.drawable.animation_events);
        animationEvents = (AnimationDrawable) imageEvents.getBackground();
        imageEvents.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                animationEvents.stop();
                animationEvents.start();

                animationHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent events = new Intent(getBaseContext(), PromosEventsBar.class);
                        events.putExtra("NAME", barName);
                        events.putExtra("EVENTS_PROMOS", "events");
                        startActivity(events);
                    }
                }, getTotalDuration(animationEvents));
            }
        });

        bottomDonPorter = (ImageView) findViewById(R.id.donPorter_bottom);

        /*barParams.addRule( RelativeLayout.BELOW, R.id.appbar_home );
        barParams.addRule( RelativeLayout.ALIGN_PARENT_LEFT );
        barParams.setMargins(marginSide, marginTop, 0, 0);
        cardBar = (CardView) findViewById(R.id.home_button_bar);
        cardBar.setLayoutParams(barParams);
        cardBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home = new Intent(getBaseContext(), InfoBar.class);
                home.putExtra("NAME", barName);
                startActivity(home);
            }
        });*/

    }

    @Override
    protected void onStop() {
        super.onStop();

        try {
            imageBar.clearAnimation();
            imageMenu.clearAnimation();
            imagePromos.clearAnimation();
            imageEvents.clearAnimation();

            animationBar.stop();
            animationMenu.stop();
            animationPromos.stop();
            animationEvents.stop();

            animationHandler.dispatchMessage(animationHandler.obtainMessage());
        } catch (Exception e) {
            Log.e(e.getMessage(), "Animation error");
        }
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putString(BAR_NAME, barName);

        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        // Always call the superclass so it can restore the view hierarchy
        super.onRestoreInstanceState(savedInstanceState);

        barName = savedInstanceState.getString(BAR_NAME);
    }

    //Get layout size
    public void layoutSize(){
        Display display = getWindowManager().getDefaultDisplay();

        size = new Point();
        display.getSize(size);

        if(getResources().getConfiguration().orientation == ActivityInfo.SCREEN_ORIENTATION_PORTRAIT ) {
//            buttonsSize = (size.x / 2) - (int) getResources||().getDimension(R.dimen.cardview_bottom_margin) - height;
            buttonsSize = (size.x / 2) - height;
//            buttonsGrid.setPadding(0,height,0,0);
        } else {
            buttonsSize = (size.y / 2) - (int) getResources().getDimension(R.dimen.cardview_bottom_margin) - height;
            buttonsGrid.setPadding(0,0,0,0);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_default, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        //noinspection SimplifiableIfStatement
        switch (item.getItemId()) {
            case R.id.action_donporter:
                Intent who = new Intent(this, DonPorter.class);
                startActivity(who);
                break;
            case R.id.action_settings:
                Intent settings = new Intent(this, SettingsActivity.class);
                startActivity(settings);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    public void settingToolBar() {
        toolbar = (Toolbar) findViewById(R.id.appbar_home);
        toolbar.setTitle(barName);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        setSupportActionBar(toolbar);
    }

    /**
     * Gets the total duration of all frames.
     *
     * @return The total duration.
     */
    public int getTotalDuration(AnimationDrawable animationDrawable) {

        int iDuration = 0;

        for (int i = 0; i < animationDrawable.getNumberOfFrames(); i++) {
            iDuration += animationDrawable.getDuration(i);
        }

        return iDuration;
    }

    /*private class LooperThread extends Thread {
        public Handler mHandler;
        Context first, second;
        String type;

        public LooperThread(Context firstActivity, Context secondActivity, String type) {
            first = firstActivity;
            second = secondActivity;
            this.type = type;
        }

        @Override
        public void run() {
            // Initialize the current thread as a Looper
            // (this thread can have a MessageQueue now)
            Looper.prepare();

            mHandler = new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    // Process incoming messages here
                    Intent next = new Intent(first, second.getClass());
                    next.putExtra("NAME", barName);
                    next.putExtra("EVENTS_PROMOS", type);
                }
            };

            Looper.loop();
        }
    }*/
}
