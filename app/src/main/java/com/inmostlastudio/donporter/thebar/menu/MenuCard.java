package com.inmostlastudio.donporter.thebar.menu;

/**
 * Created by IN MOSTLA Studio on 12/11/2015.
 *
 * Clase para el Adapter en el RecyclerView de la lista del Menú.
 */

public class MenuCard {

    private String mItem, mDescription;
    private int mPrice = 0;
    private boolean mHeader;

    public MenuCard( ){
        mItem = "";
        mDescription = "";
        mPrice = 0;
        mHeader = false;
    }

    public MenuCard( String item, String description, int price ) {
        mItem = item;
        mDescription = description;
        mPrice = price;
    }

    public MenuCard (String header) {
        mItem = header;
        mHeader = true;
    }

    public void setItem(String item) {
        mItem = item;
    }
    public void setDescription(String description) {
        mDescription = description;
    }
    public void setPrice(int price) {
        mPrice = price;
    }
    public void setHeader(boolean header) {
        mHeader = header;
    }

    public String getItem() {
        return mItem;
    }
    public String getDescription() {
        return mDescription;
    }
    public int getPrice() {
        return mPrice;
    }
    public boolean isHeader() {
        return mHeader;
    }
}
