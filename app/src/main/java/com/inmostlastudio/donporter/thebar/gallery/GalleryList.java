package com.inmostlastudio.donporter.thebar.gallery;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import com.inmostlastudio.donporter.R;

import java.util.List;

/**
 * Created by IN MOSTLA Studio on 25/11/2015.
 *
 */
public class GalleryList extends BaseAdapter {

    Context mContext;
    LayoutInflater inflater;
    private List<String> photoList;

    public GalleryList(Context context, List<String> gallery) {
        mContext = context;
        photoList = gallery;
        inflater = LayoutInflater.from(context);
    }

    public class ViewHolder {
        ImageView photo;
    }

    @Override
    public int getCount() {
        return photoList.size();
    }

    @Override
    public Object getItem(int position) {
        return photoList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if(convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.gallery_item, null);

            holder.photo = (ImageView) convertView.findViewById(R.id.gallery_photo);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Picasso.with(mContext).load(photoList.get(position)).resize(300,300).into(holder.photo);

        return convertView;
    }

}
