package com.inmostlastudio.donporter.thebar.promotions;

/**
 * Created by IN MOSTLA Studio on 17/11/2015.
 *
 */

public class PromosEventsCard {

    String title, description, image;

    public PromosEventsCard() {
        title = "";
        image = "";
        description = "";
    }
    public PromosEventsCard(String name, String url) {

        title = name;
        image = url;
    }

    public void setTitle(String name) {
        title = name;
    }
    public void setImage(String url) {
        image = url;
    }
    public void setDescription(String content) {
        description = content;
    }

    public String getImage() {
        return image;
    }
    public String getTitle() {
        return title;
    }
    public String getDescription() {
        return description;
    }

}
