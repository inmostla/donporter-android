package com.inmostlastudio.donporter.contact;

import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.inmostlastudio.donporter.R;
import com.inmostlastudio.donporter.settings.SettingsActivity;
import com.inmostlastudio.donporter.utils.Utils;

/**
 * Created by IN MOSTLA Studio on 05/01/2016.
 *
 *
 */

public class DonPorter extends AppCompatActivity {

    int toolbarHeight, statusbarHeight, height;
    Point size;
    Toolbar toolbar;

    String userFacebookName, userFacebookId, userTwitter, userInstagram;
    TextView info_donporter_text;
    ImageView info_donporter_image, inmostla_image;
    ImageView facebook, twitter, instagram;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_don_porter);
        settingToolBar();
        layoutSize();

        // Set the padding to match the Status Bar height
        toolbarHeight = Utils.getToolbarHeight(this);
        statusbarHeight = Utils.getStatusBarHeight(this);
        // Si se encuentra dentro del rango de la versión.
        if(Build.VERSION.SDK_INT >= 21 || Build.VERSION.SDK_INT <= 17 ) {
            toolbar.setPadding(0, 0, 0, 0);     //Por el tipo de versión se toman diferentes paddings
            height = toolbarHeight;
        } else {
            toolbar.setPadding(0, statusbarHeight, 0, 0);
            height = toolbarHeight + statusbarHeight;
        }

        info_donporter_text = (TextView) findViewById(R.id.info_donporter_text);
        info_donporter_text.getLayoutParams().width = size.x/2;
        info_donporter_image = (ImageView) findViewById(R.id.info_donporter_image);
        info_donporter_image.getLayoutParams().width = size.x/2;

        inmostla_image = (ImageView) findViewById(R.id.inmostla_created);
        inmostla_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent inmostla = new Intent(DonPorter.this, ContactInMostla.class);
                startActivity(inmostla);
            }
        });

        userFacebookId = getResources().getString(R.string.don_porter_facebook_id);
        userFacebookName = getResources().getString(R.string.don_porter_facebook_page);
        userTwitter = getResources().getString(R.string.don_porter_twitter);
        userInstagram = getResources().getString(R.string.don_porter_instagram);

        facebook = (ImageView) findViewById(R.id.icon_social_facebook);
        facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(getOpenFacebookIntent(DonPorter.this));
            }
        });
        twitter = (ImageView) findViewById(R.id.icon_social_twitter);
        twitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(getOpenTwitterIntent(DonPorter.this));
            }
        });
        instagram = (ImageView) findViewById(R.id.icon_social_instagram);
        instagram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(getOpenInstagramIntent(DonPorter.this));
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_default, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        switch (item.getItemId()) {
            case R.id.action_contact_inmostla:
                Intent inmostla = new Intent(this, ContactInMostla.class);
                startActivity(inmostla);
                break;
            case R.id.action_settings:
                Intent settings = new Intent(this, SettingsActivity.class);
                startActivity(settings);
                break;
        }

        return super.onOptionsItemSelected(item);
    }


    public void settingToolBar() {
        toolbar = (Toolbar) findViewById(R.id.appbar_donporter);
        toolbar.setTitle("Don Porter");
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        setSupportActionBar(toolbar);
    }

    //Get layout size
    public void layoutSize(){
        Display display = getWindowManager().getDefaultDisplay();

        size = new Point();
        display.getSize(size);
        size.y = height;
        size.x = size.x - 2*(int)getResources().getDimension(R.dimen.activity_horizontal_margin);
    }

    public Intent getOpenFacebookIntent(Context context) {
        try {
            context.getPackageManager().getPackageInfo("com.facebook.katana", 0);
            return new Intent(Intent.ACTION_VIEW, Uri.parse("fb://page/" + userFacebookId));
        } catch (Exception e) {
            return new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://www.facebook.com/" + userFacebookName));
        }
    }

    public Intent getOpenTwitterIntent(Context context) {
        try {
            context.getPackageManager().getPackageInfo("com.twitter.android", 0);
            return new Intent(Intent.ACTION_VIEW, Uri.parse("twitter://user?screen_name=" + userTwitter));
        } catch (Exception e) {
            return new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://twitter.com/" + userTwitter));
        }
    }

    public Intent getOpenInstagramIntent(Context context) {
        try {
            context.getPackageManager().getPackageInfo("com.instagram.android",0);
            return new Intent(Intent.ACTION_VIEW, Uri.parse("http://instagram.com/_u/" + userInstagram));
        } catch (Exception e) {
            return new Intent(Intent.ACTION_VIEW, Uri.parse("http://instagram.com/" + userInstagram));
        }
    }
}
