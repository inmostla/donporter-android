package com.inmostlastudio.donporter.contact;

import android.content.Intent;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;

import com.inmostlastudio.donporter.R;
import com.inmostlastudio.donporter.settings.SettingsActivity;
import com.inmostlastudio.donporter.utils.Utils;

/**
 * Created by IN MOSTLA Studio on 07/08/2015.
 *
 * Vista del contacto de IN MOSTLA Studio y de Don Porter.
 */

public class ContactInMostla extends AppCompatActivity {

    int toolbarHeight, statusbarHeight, height;
    Point size;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_inmostla);
        settingToolBar();
        layoutSize();

        // Set the padding to match the Status Bar height
        toolbarHeight = Utils.getToolbarHeight(this);
        statusbarHeight = Utils.getStatusBarHeight(this);
        // Si se encuentra dentro del rango de la versión.
        if(Build.VERSION.SDK_INT >= 21 || Build.VERSION.SDK_INT <= 17 ) {
            toolbar.setPadding(0, 0, 0, 0);     //Por el tipo de versión se toman diferentes paddings
            height = toolbarHeight;
        } else {
            toolbar.setPadding(0, statusbarHeight, 0, 0);
            height = toolbarHeight + statusbarHeight;
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_default, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        switch (item.getItemId()) {
            case R.id.action_donporter:
                Intent donporter = new Intent(this, DonPorter.class);
                startActivity(donporter);
                break;
            case R.id.action_settings:
                Intent settings = new Intent(this, SettingsActivity.class);
                startActivity(settings);
                break;
        }

        return super.onOptionsItemSelected(item);
    }


    public void settingToolBar() {
        toolbar = (Toolbar) findViewById(R.id.appbar_inmostla);
        toolbar.setTitle("IN MOSTLA Studio");
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        setSupportActionBar(toolbar);
    }

    //Get layout size
    public void layoutSize(){
        Display display = getWindowManager().getDefaultDisplay();

        size = new Point();
        display.getSize(size);
    }

}
