package com.inmostlastudio.donporter.barlistcontents;

import android.content.res.Configuration;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.inmostlastudio.donporter.adapter.BarsSearchAdapter;

import java.util.ArrayList;
import java.util.List;

import com.inmostlastudio.donporter.R;
import com.inmostlastudio.donporter.barsInfo.TheBars;
import com.inmostlastudio.donporter.utils.Utils;

/**
 * Created by IN MOSTLA Studio on 04/09/2015.
 *
 * Sistema de b~squeda de los bares en una vista simplificada
 *
 */

public class BarSearchActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {

    //Variables
    int toolbarHeight, statusbarHeight, height;
    Point size;     // Para conocer las medidas de la pantalla

    private List<BarSearchCard> barSearch;
    Toolbar toolbar;
    RecyclerView recyclerView;
    BarsSearchAdapter barsSearchAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        //Inicia los elementos de la lista
        initializeData();
        //Inicia el toolbar
        settingToolBar();
        // Obtiene el tamaño de la pantalla
        // Get layout size
        layoutSize();

        // Set the padding to match the StatusBar & ToolBar height
        toolbarHeight = Utils.getToolbarHeight(this);
        statusbarHeight = Utils.getStatusBarHeight(this);
        // Si se encuentra dentro del rango de la versión.
        if(Build.VERSION.SDK_INT >= 21 || Build.VERSION.SDK_INT <= 17 ) {
            toolbar.setPadding(0, 0, 0, 0); //Por el tipo de versión se toman diferentes paddings
            height = toolbarHeight;
        } else {
            toolbar.setPadding(0, statusbarHeight, 0, 0);
            height = toolbarHeight + statusbarHeight;
        }
        //Buscar el Recycler en el layout
        recyclerView = (RecyclerView) findViewById(R.id.bars_search_list);

        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            LinearLayout.LayoutParams layoutParams =
                    new LinearLayout.LayoutParams(recyclerView.getLayoutParams());
            //layoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
            layoutParams.setMargins(size.x / 9, 0, 0, 0);
            recyclerView.setLayoutParams(layoutParams);
        }

        //Crea un layout manager para el Recycler del search
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(mLayoutManager);

        //Crea el adaptador con la lista de bares
        barsSearchAdapter = new BarsSearchAdapter(this, barSearch, size);
        //Agrega el adaptador al Recycler
        recyclerView.setAdapter(barsSearchAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);

        //Inicializa el item de search del menú
        MenuItem item = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setIconifiedByDefault(false);
        searchView.setOnQueryTextListener(this);

        return true;
    }

    /* Inicializa el toolbar */
    public void settingToolBar() {
        toolbar = (Toolbar) findViewById(R.id.appbar_search);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        setSupportActionBar(toolbar);
    }

    /* Listener para la entrada de texto desde el menú */
    @Override
    public boolean onQueryTextChange(String query) {
        final List<BarSearchCard> filteredSearch = filter(barSearch, query);
        barsSearchAdapter.animateTo(filteredSearch);
        recyclerView.scrollToPosition(0);
        return true;
    }

    /* Método en caso del envío de texto desde el menú */
    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    /* Filtro al momento de la entrada de texto desde el menú */
    private List<BarSearchCard> filter(List<BarSearchCard> bars, String query ) {
        query = query.toLowerCase();

        final List<BarSearchCard> filteredSearch = new ArrayList<>();
        for(BarSearchCard search : bars) {
            final String text = search.getBarName().toLowerCase();
            if(text.contains(query)) {
                filteredSearch.add(search);
            }
        }

        return filteredSearch;
    }

    /* Inicialización la lista de bares */
    void initializeData() {
        barSearch = new ArrayList<>();

        TheBars.BarINMOSTLA();
        barSearch.add(getNewSearchCard());

        TheBars.BarUno();
        barSearch.add(getNewSearchCard());

        TheBars.BarDos();
        barSearch.add(getNewSearchCard());

        TheBars.BarTres();
        barSearch.add(getNewSearchCard());

        TheBars.BarCuatro();
        barSearch.add(getNewSearchCard());

        TheBars.BarCinco();
        barSearch.add(getNewSearchCard());
    }

    /* Return de una nueva tarjeta de bares para la lista */
    BarSearchCard getNewSearchCard() {
        return new BarSearchCard(TheBars.getBarName(),
                TheBars.getCardView_motto(), TheBars.getLogoId());
    }

    // Get layout size
    public void layoutSize(){
        Display display = getWindowManager().getDefaultDisplay();
        size = new Point();
        display.getSize(size);
    }
}
