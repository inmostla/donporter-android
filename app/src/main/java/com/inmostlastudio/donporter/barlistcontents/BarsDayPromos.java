package com.inmostlastudio.donporter.barlistcontents;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import com.inmostlastudio.donporter.R;
import com.inmostlastudio.donporter.adapter.DayPromosAdapter;
import com.inmostlastudio.donporter.contact.DonPorter;
import com.inmostlastudio.donporter.settings.SettingsActivity;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

public class BarsDayPromos extends AppCompatActivity {

    String barName, weekDay;
    Toolbar toolbar;

    ProgressDialog mProgressDialog;
    private List<DayPromosCard> list;
    List<ParseObject> objList;
    ListView listView;
    DayPromosAdapter adapter;

    int day;
    Calendar calendar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daypromos);

        if( getIntent().getExtras() != null ) {
            Bundle extras = getIntent().getExtras();
            barName = extras.getString("NAME");
        }
        settingToolBar();
        setWeekDay();

        new RemoteData().execute();
    }

    private class RemoteData extends AsyncTask< Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog = new ProgressDialog(BarsDayPromos.this);
            mProgressDialog.setTitle(weekDay);
            // Set progressdialog message
            mProgressDialog.setMessage("Cargando...");
            mProgressDialog.setIndeterminate(false);
            // Show progressdialog
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            list = new ArrayList<>();
            try {
                ParseQuery<ParseObject> query = new ParseQuery<>("DayPromos");
                query.orderByAscending("day");
                objList = query.find();
                for(ParseObject item : objList) {
                    if( ( item.get("day")).equals(weekDay)) {
                        ParseFile image = (ParseFile) item.get("logo");
                        DayPromosCard promosCard = new DayPromosCard(item.get("bar").toString(),
                                item.get("description").toString(), image.getUrl());
                        list.add(promosCard);
                    }
                }
            } catch (ParseException e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            listView = (ListView) findViewById(R.id.daypromos_list);
            adapter = new DayPromosAdapter(BarsDayPromos.this, list);

            listView.setAdapter(adapter);
            mProgressDialog.dismiss();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_default, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_donporter:
                Intent who = new Intent(this, DonPorter.class);
                startActivity(who);
                break;
            case R.id.action_settings:
                Intent settings = new Intent(this, SettingsActivity.class);
                startActivity(settings);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void settingToolBar() {
        toolbar = (Toolbar) findViewById(R.id.appbar_promosday);
        toolbar.setTitle("Las de Hoy");
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        setSupportActionBar(toolbar);
    }

    public void setWeekDay() {
        calendar = Calendar.getInstance(TimeZone.getDefault());
        day = calendar.get(Calendar.DAY_OF_WEEK);
        switch (day) {
            case Calendar.MONDAY: weekDay = "Lunes";
                break;
            case Calendar.TUESDAY: weekDay = "Martes";
                break;
            case Calendar.WEDNESDAY: weekDay = "Miércoles";
                break;
            case Calendar.THURSDAY: weekDay = "Jueves";
                break;
            case Calendar.FRIDAY: weekDay = "Viernes";
                break;
            case Calendar.SATURDAY: weekDay = "Sábado";
                break;
            case Calendar.SUNDAY: weekDay = "Domingo";
                break;
        }
    }

}
