package com.inmostlastudio.donporter.barlistcontents;

/**
 * Created by IN MOSTLA Studio on 05/01/2016.
 *
 */

public class DayPromosCard {

    private String barName, promoDescription, barLogoUrl;

    /* Constructors */
    public DayPromosCard() {
        barName = "";
        promoDescription = "";
        barLogoUrl = "";
    }
    public DayPromosCard(String name, String description, String logo) {
        barName = name;
        promoDescription = description;
        barLogoUrl = logo;
    }

    public String getBarName() {
        return barName;
    }
    public String getPromoDescription() {
        return promoDescription;
    }
    public String getBarLogoUrl() {
        return barLogoUrl;
    }
}
