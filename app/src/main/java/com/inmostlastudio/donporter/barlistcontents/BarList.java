package com.inmostlastudio.donporter.barlistcontents;

/*
    Copyright (C) 2014 Jerzy Chalupski

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.inmostlastudio.donporter.adapter.BarsRecyclerAdapter;
import com.inmostlastudio.donporter.barsInfo.TheBars;
import com.inmostlastudio.donporter.contact.ContactInMostla;
import com.inmostlastudio.donporter.contact.DonPorter;
import com.inmostlastudio.donporter.settings.SettingsActivity;
import com.inmostlastudio.donporter.utils.MyRecyclerScroll;
import com.inmostlastudio.donporter.utils.Utils;
import com.getbase.floatingactionbutton.FloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionsMenu;

import java.util.ArrayList;
import java.util.List;

import com.inmostlastudio.donporter.R;

/**
 * Created by IN MOSTLA Studio on 06/08/2015.
 *
 * Lista de todos los bares en la aplicación.
 *
 */

// TODO DESIGN: 14/10/2015 animación al click de las tarjetas
// TODO DESIGN: 14/10/2015 Espacio al final del RecyclerView para imagen de fondo

public class BarList extends AppCompatActivity {

    //Variables
    SharedPreferences preferences;      // Preferencias para los favoritos y lista de deseos
    static List<BarCard> barList, barSearch;    // Lista de bares

    int toolbarHeight, statusbarHeight, height;     // Medidas del toolbar
    TextView text;      // Texto cuando no hay elementos disponibles
    Point size;     // Para conocer las medidas de la pantalla
    Toolbar toolbar;    // Toolbar
    CoordinatorLayout coordinatorLayout; // Coordinator para manejar un Snackbar junto con el FAB
    LinearLayout toolbarContainer;  // LinearLayout para realizar el coorDinatorLayout y esconder el toolbar
    RecyclerView mRecyclerView;     // Recycler donde se almacenarán la lista de bares
    BarsRecyclerAdapter barsRecyclerAdapter;    // Adaptador
    FloatingActionButton fab_favorites, fab_wishlist, fab_barList;  // Fabs del menú
    FloatingActionsMenu floatingActionsMenu;    // Menú de FAB para ver la lista completa,
                                                // los favoritos y la lista de deseos.

    /* Constructor */
    public BarList() {
        barList = new ArrayList<>();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        settingToolBar();   // Inicializa el toolbar
        layoutSize();   // Establece las medidas de la pantalla

        // Set the padding to match the Status Bar height
        toolbarHeight = Utils.getToolbarHeight(this);
        statusbarHeight = Utils.getStatusBarHeight(this);
        // Si se encuentra dentro del rango de la versión.
        if(Build.VERSION.SDK_INT >= 21 || Build.VERSION.SDK_INT <= 17 ) {
            toolbar.setPadding(0, 0, 0, 0);     //Por el tipo de versión se toman diferentes paddings
            height = toolbarHeight;
        } else {
            toolbar.setPadding(0, statusbarHeight, 0, 0);
            height = toolbarHeight + statusbarHeight;
        }

        // Abre el archivo de las preferencias para favoritos y whislist
        preferences = this.getSharedPreferences("FAV_WISH_FILE", Context.MODE_PRIVATE);

        // Inicializa los datos de los bares
        initializeData();

        // Busca los respectivos elementos en el layout
        toolbarContainer = (LinearLayout) findViewById(R.id.fabhide_toolbar_container);
        mRecyclerView = (RecyclerView) findViewById(R.id.bars_recycler_view);
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinator_list);

        // Busca el textView para el texto de 'no contenido'
        text = (TextView) findViewById(R.id.no_content_text);

        // Establece los valores del Recycler
        mRecyclerView.setPadding(0, height + 10, 0, 0);
        mRecyclerView.setHasFixedSize(true);

        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            RelativeLayout.LayoutParams layoutParams =
                    new RelativeLayout.LayoutParams(mRecyclerView.getLayoutParams());
            layoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
            layoutParams.setMargins(size.x/6,0,0,0);
            mRecyclerView.setLayoutParams(layoutParams);
        }

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(mLayoutManager);

        // Crea un Adapter con la lista de bares inicializada
        barsRecyclerAdapter = new BarsRecyclerAdapter(this, barList, size, coordinatorLayout);
        mRecyclerView.setAdapter(barsRecyclerAdapter);

        // Agrega un scrollListener para saber cuando esconder/mostar el FAB y el toolbar
        mRecyclerView.addOnScrollListener(new MyRecyclerScroll() {
            @Override
            public void show() {
                toolbarContainer.animate().translationY(0)
                        .setInterpolator(new DecelerateInterpolator(2)).start();
                floatingActionsMenu.animate().translationY(0)
                        .setInterpolator(new DecelerateInterpolator(2)).start();
            }

            @Override
            public void hide() {
                toolbarContainer.animate().translationY(-toolbarHeight - statusbarHeight)
                        .setInterpolator(new AccelerateInterpolator(2)).start();
                if (floatingActionsMenu.isExpanded())
                    floatingActionsMenu.collapse();
                floatingActionsMenu.animate().translationY(floatingActionsMenu.getHeight()
                        + getResources().getDimensionPixelSize(R.dimen.fab_margin))
                        .setInterpolator(new AccelerateInterpolator(2)).start();
            }
        });

        // Busca el FAB dentro del layout
        floatingActionsMenu = (FloatingActionsMenu) findViewById(R.id.fab_list);

        // Busca e inicia las opciones el FAB
        fab_barList = (FloatingActionButton) findViewById(R.id.fab_bar_list);
        fab_barList.setVisibility( View.GONE );
        fab_barList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FavWish(0);
                fab_barList.setVisibility(View.GONE);
                fab_favorites.setVisibility( View.VISIBLE );
                fab_wishlist.setVisibility(  View.VISIBLE );
                floatingActionsMenu.collapse();
            }
        });
        fab_favorites = (FloatingActionButton) findViewById(R.id.fab_favorites);
        fab_favorites.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FavWish(1);
                fab_favorites.setVisibility(View.GONE);
                fab_barList.setVisibility( View.VISIBLE );
                fab_wishlist.setVisibility(View.VISIBLE);
                floatingActionsMenu.collapse();
            }
        });
        fab_wishlist = (FloatingActionButton) findViewById(R.id.fab_wishlist);
        fab_wishlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FavWish(2);
                fab_wishlist.setVisibility(View.GONE);
                fab_barList.setVisibility( View.VISIBLE );
                fab_favorites.setVisibility(View.VISIBLE);
                floatingActionsMenu.collapse();
            }
        });

    }

    /* Creación del menú superior */
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu_bar_list, menu);

        return true;
    }

    /* Maneja las opciones del menú superior */
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()) {
            case R.id.action_search:
                Intent search = new Intent(this, BarSearchActivity.class);
                startActivity(search);
                break;
            case R.id.action_donporter:
                Intent who = new Intent(this, DonPorter.class);
                startActivity(who);
                break;
            case R.id.action_settings:
                Intent settings = new Intent(this, SettingsActivity.class);
                startActivity(settings);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    /* Inicia el toolbar */
    public void settingToolBar() {
        toolbar = (Toolbar) findViewById(R.id.appbar_list);
        toolbar.setTitle(R.string.activity_barList);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        setSupportActionBar(toolbar);
    }

    /* Método utilizado para el filtro de búsqueda en las opciones del FAB */
    public void FavWish (int type) {
        barList.clear();
        initializeData();
        barSearch = new ArrayList<>(barList);

        final List<BarCard> filteredSearch = filter(barSearch, type);
        barsRecyclerAdapter.animateTo(filteredSearch);
        if( filteredSearch.isEmpty() ){
            text.setText(getString(R.string.no_content_text));
        }
        else {
            text.setText("");
        }

        mRecyclerView.scrollToPosition(0);
    }

    /* Lista de bares utilizadas como filtro para la vista de Favoritos, Wishlist y completos */
    private List<BarCard> filter(List<BarCard> bars, int type ) {
        final List<BarCard> filteredSearch = new ArrayList<>();

        for(BarCard search : bars) {
            switch (type) {
                case 0:
                    filteredSearch.add(search);
                    break;
                case 1:
                    if(search.getFavState()) {
                        filteredSearch.add(search);
                    }
                    break;
                case 2:
                    if(search.getWishState()) {
                        filteredSearch.add(search);
                    }
                    break;
            }

        }

        return filteredSearch;
    }

    /* Inicia los valores del la lista de bares */
    private void initializeData() {

        TheBars.BarINMOSTLA();
        TheBars.setPrefState(this);
        barList.add(getNewCard());

        TheBars.BarUno();
        TheBars.setPrefState(this);
        barList.add(getNewCard());

        TheBars.BarDos();
        TheBars.setPrefState(this);
        barList.add(getNewCard());

        TheBars.BarTres();
        TheBars.setPrefState(this);
        barList.add(getNewCard());

        TheBars.BarCuatro();
        TheBars.setPrefState(this);
        barList.add(getNewCard());

        TheBars.BarCinco();
        TheBars.setPrefState(this);
        barList.add(getNewCard());

    }

    // Get layout size
    public void layoutSize(){
        Display display = getWindowManager().getDefaultDisplay();
        size = new Point();
        display.getSize(size);
    }

    /* Obtiene una nueva tarjeta desde la clase TheBars con el bar siguiente */
    BarCard getNewCard() {
        return new BarCard(TheBars.getBarName(), TheBars.getImageId(),
                TheBars.getLogoId(), TheBars.getCardView_motto(), TheBars.getFavState(),
                TheBars.getWishState(), TheBars.getPref_Fav(), TheBars.getPref_Wish());
    }

}
