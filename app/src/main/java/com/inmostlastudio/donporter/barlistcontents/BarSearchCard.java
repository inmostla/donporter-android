package com.inmostlastudio.donporter.barlistcontents;

/**
 * Created by IN MOSTLA Studio on 04/09/2015.
 *
 * CardView de los bares simplificada, para el sistema de b~squeda
 */

public class BarSearchCard {
    private final String barName, barMotto;
    private final int logoId;

    public BarSearchCard(String name, String motto, int logo) {
        barName = name;
        barMotto = motto;
        logoId = logo;
    }

    public String getBarName() {
        return barName;
    }
    public String getBarMotto() {
        return barMotto;
    }
    public int getLogoId() {
        return logoId;
    }
}
