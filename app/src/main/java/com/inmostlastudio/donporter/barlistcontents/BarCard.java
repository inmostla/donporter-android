package com.inmostlastudio.donporter.barlistcontents;

/**
 * Created by IN MOSTLA Studio on 18/08/2015.
 *
 * Class with the necessar data to display a CardView in the BarList.
 *
 */

public class BarCard {

    private String name, motto;
    private String p_fav, p_wish;
    private int imageId, logoId;
    private boolean fav_state, wish_state;

    /* Constructor */
    public BarCard(String name, int imageId, int logoId, String motto,
                   boolean fav_state, boolean wish_state, String fav_pref, String wish_pref) {
        this.imageId = imageId;
        this.logoId = logoId;
        this.name = name;
        this.motto = motto;
        this.fav_state = fav_state;
        this.wish_state = wish_state;
        this.p_fav = fav_pref;
        this.p_wish = wish_pref;
    }
    public void setFavWish(boolean fav, boolean wish) {
        fav_state = fav;
        wish_state = wish;
    }

    /* Métodos para retorno de valores */
    public String getName() {
        return name;
    }
    public int getImageId() {
        return imageId;
    }
    public int getLogoId() {
        return logoId;
    }
    public String getMotto() {
        return motto;
    }
    public boolean getFavState() {
        return fav_state;
    }
    public boolean getWishState() {
         return wish_state;
    }
    public String getPref_fav() {
        return p_fav;
    }
    public String getPref_wish() {
        return p_wish;
    }

}
