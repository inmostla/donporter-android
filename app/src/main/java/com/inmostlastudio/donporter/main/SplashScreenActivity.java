package com.inmostlastudio.donporter.main;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import com.inmostlastudio.donporter.R;
import com.inmostlastudio.donporter.thebar.menu.MenuCard;
import com.inmostlastudio.donporter.thebar.menu.MenuSectionedList;
import com.inmostlastudio.donporter.utils.Utils;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

/**
 * Created by IN MOSTLA Studio on 04/08/2015.
 *
 * Sets the first SplashScreen of the App.
 *
 */

public class SplashScreenActivity extends Activity {

    // Set the duration of the splash screen
    private static final long SPLASH_SCREEN_DELAY = 3000;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Set portrait orientation
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        // Hide title bar
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.splashscreen_inmostla);

        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                // Start the next activity
                Intent main = new Intent(SplashScreenActivity.this, MainActivity.class);
                startActivity(main);

                // Close the activity so the user won't able to go back this
                // activity pressing Back button
                finish();
            }
        };

        // Simulate a long loading process on application startup.
        Timer timer = new Timer();
        timer.schedule(task, SPLASH_SCREEN_DELAY);
        new RemoteDataVersion().execute();
    }

    private class RemoteDataVersion extends AsyncTask< Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            try {
                //Localizar la tabla en Parse
                ParseQuery<ParseObject> query = new ParseQuery<>("Settings");
                query.whereEqualTo("objectId", "1Qa4aJjO4n");
                List<ParseObject> obj = query.find();
                Utils.setLatest(obj.get(0).getBoolean("latest_version"));
            } catch (ParseException e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }

            return null;
        }

    }

}
