package com.inmostlastudio.donporter.main;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Point;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Display;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.inmostlastudio.donporter.barlistcontents.BarList;
import com.inmostlastudio.donporter.barlistcontents.BarsDayPromos;
import com.inmostlastudio.donporter.contact.ContactInMostla;
import com.inmostlastudio.donporter.contact.DonPorter;
import com.inmostlastudio.donporter.settings.SettingsActivity;
import com.inmostlastudio.donporter.utils.Utils;

import com.inmostlastudio.donporter.R;
import com.parse.ParseAnalytics;

/**
 * Created by IN MOSTLA Studio on 04/08/2015.
 *
 * MainActivity.
 * Muestra el logo de la aplicación.
 * Botón para entrar a la Lista de Bares.
 * Botón para entrar a las promociones del día.
 * Botón para entrar al contacto "¿Quien Soy?" de Don Porter.
 * Boton de Settings de la aplicación.
 *
 */

// TODO: 12/11/2015 Several APK production files
// TODO: 22/11/2015 Serialize datos de bares

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Point size;
    int toolbarHeight, statusbarHeight, height;     //Medidas del toolbar
    //Toolbar agregado en el layout
    private Toolbar toolbar;

    ImageView bars, promosList, settings, who;
    ImageView donPorter, donPorterFont;

    /*Constructor*/
    public MainActivity() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Inicia el Toolbar
        settingToolBar();
        // Set the padding to match the Status Bar height
        toolbarHeight = Utils.getToolbarHeight(this);
        statusbarHeight = Utils.getStatusBarHeight(this);
        //Si se encuentra dentro del rango de la versión.
        if(Build.VERSION.SDK_INT >= 21 || Build.VERSION.SDK_INT <= 17 ) {
            toolbar.setPadding(0, 0, 0, 0);     //Por el tipo de versión se toman diferentes paddings
            height = toolbarHeight;
        }
        else {
            toolbar.setPadding(0, statusbarHeight, 0, 0);
            height = toolbarHeight + statusbarHeight;
        }
        // Obtiene el tamaño de la pantalla
        layoutSize();

        // Inicia las imágenes
        donPorter = (ImageView) findViewById(R.id.donPorter_logo);
        donPorterFont = (ImageView) findViewById(R.id.donPorter_font);

        // Inicia los botones
        bars = (ImageView) findViewById(R.id.main_home_icon);
        bars.setOnClickListener(this);
        promosList = (ImageView) findViewById(R.id.promos_list_icon);
        promosList.setOnClickListener(this);
        settings  = (ImageView) findViewById(R.id.settings_icon);
        settings.setOnClickListener(this);
        who = (ImageView) findViewById(R.id.who_icon);
        who.setOnClickListener(this);

        setDimensions();
        ParseAnalytics.trackAppOpenedInBackground(getIntent());
    }

    @Override
    public void onResume() {
        super.onResume();

        /** Verifica el estado de conexiÃ³n del dispositivo */
        if (!Utils.verificaConexion(this)) {
            Toast.makeText(getBaseContext(),
                    "No cuentas con conexión a Internet", Toast.LENGTH_LONG)
                    .show();
        }

        if(!Utils.isLatestVersion()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Hay una nueva versión disponible en Play Store.")
                    .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            startActivity(new Intent(Intent.ACTION_VIEW,
                                    Uri.parse("market://details?id=" + "inmostla.ligatangamanga")));
                        }
            }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    System.exit(0);
                }
            }).setOnKeyListener(new DialogInterface.OnKeyListener() {
                @Override
                public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        System.exit(0);
                    }
                    return true;
                }
            });

            // Run the AlertDialog
            builder.create().show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        //noinspection SimplifiableIfStatement
        switch (item.getItemId()) {
            //Handle para la opción 'contacto' del menú
            case R.id.action_contact_inmostla:
                Intent inmostla = new Intent(this, ContactInMostla.class);
                startActivity(inmostla);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    /* Maneja en un solo onClick el botón que se hizo click. */
    @Override
    public void onClick(View v){
        switch (v.getId())
        {
            case R.id.main_home_icon:
                Intent barL = new Intent(this, BarList.class);
                startActivity(barL);
                break;
            case R.id.promos_list_icon:
                Intent promos = new Intent(this, BarsDayPromos.class);
                startActivity(promos);
                break;
            case R.id.who_icon:
                Intent who = new Intent(this, DonPorter.class);
                startActivity(who);
                break;
            case R.id.settings_icon:
                Intent settings = new Intent(this, SettingsActivity.class);
                startActivity(settings);
                break;
        }

    }

    /* Inicializa el toolbar agregado programaticamente */
    public void settingToolBar() {
        //getWindow().setStatusBarColor(Color.TRANSPARENT);
        toolbar = (Toolbar) findViewById(R.id.appbar_main);     //Encuentra el toolbar en el layout
        toolbar.setTitle("");   //Título en blanco
        setSupportActionBar(toolbar);   //Agrega el toolbar
    }

    /* Get layout size */
    public void layoutSize(){
        Display display = getWindowManager().getDefaultDisplay();
        size = new Point();
        display.getSize(size);
        Utils.setScreen(size);
    }

    /* Establece los tamaños de los elementos del layout */
    public void setDimensions() {
        switch (getResources().getConfiguration().orientation) {
            case ActivityInfo.SCREEN_ORIENTATION_PORTRAIT:
                donPorter.getLayoutParams().height = size.y / 3;
                donPorter.setMaxHeight(size.y / 3);
                /*donPorterFont.getLayoutParams().width = size.x
                    - 2*(int)getResources().getDimension(R.dimen.activity_horizontal_margin);*/
                donPorterFont.setMaxHeight(size.y / 7);
                donPorterFont.setMaxWidth(size.x
                        - 2 * (int) getResources().getDimension(R.dimen.activity_horizontal_margin));

                bars.getLayoutParams().height = size.x / 4
                        - (int) getResources().getDimension(R.dimen.main_text_margin)
                        - (int) getResources().getDimension(R.dimen.activity_horizontal_margin);
                promosList.getLayoutParams().height = size.x / 4
                        - (int) getResources().getDimension(R.dimen.main_text_margin)
                        - (int) getResources().getDimension(R.dimen.activity_horizontal_margin);
                who.getLayoutParams().height = size.x / 4
                        - (int) getResources().getDimension(R.dimen.main_text_margin)
                        - (int) getResources().getDimension(R.dimen.activity_horizontal_margin);
                settings.getLayoutParams().height = size.x / 4
                        - (int) getResources().getDimension(R.dimen.main_text_margin)
                        - (int) getResources().getDimension(R.dimen.activity_horizontal_margin);

                break;
            case ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE:
                donPorter.getLayoutParams().height = size.y / 2;
                donPorter.setMaxHeight(size.y - (int) getResources().getDimension(R.dimen.main_font_height_land));
                /*donPorterFont.getLayoutParams().width = size.x
                    - 2*(int)getResources().getDimension(R.dimen.activity_horizontal_margin);*/
                donPorterFont.setMaxHeight(size.y / 7);
                donPorterFont.setMaxWidth(size.x/2
                        - 2 * (int) getResources().getDimension(R.dimen.activity_horizontal_margin));

                bars.getLayoutParams().height = size.x / 8
                        - (int) getResources().getDimension(R.dimen.main_text_margin)
                        - (int) getResources().getDimension(R.dimen.activity_horizontal_margin);
                promosList.getLayoutParams().height = size.x / 8
                        - (int) getResources().getDimension(R.dimen.main_text_margin)
                        - (int) getResources().getDimension(R.dimen.activity_horizontal_margin);
                who.getLayoutParams().height = size.x / 8
                        - (int) getResources().getDimension(R.dimen.main_text_margin)
                        - (int) getResources().getDimension(R.dimen.activity_horizontal_margin);
                settings.getLayoutParams().height = size.x / 8
                        - (int) getResources().getDimension(R.dimen.main_text_margin)
                        - (int) getResources().getDimension(R.dimen.activity_horizontal_margin);

                break;
        }
    }

}
