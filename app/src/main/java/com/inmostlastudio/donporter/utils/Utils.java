package com.inmostlastudio.donporter.utils;

import android.content.Context;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.Display;

import java.util.List;

import com.inmostlastudio.donporter.R;
import com.inmostlastudio.donporter.barlistcontents.BarCard;

/**
 * Created by IN MOSTLA Studio on 18/08/2015.
 *
 * Utilities class
 */

public class Utils {

    List<BarCard> barSearch;
    static Boolean latest = true;
    static Point screen;

    // A method to find height of the status bar
    public static int getStatusBarHeight(Context context) {
        int result = 0;

        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = context.getResources().getDimensionPixelSize(resourceId);
        }

        return result;
    }

    public static int getToolbarHeight(Context context) {
        return (int) context.getResources().getDimension(R.dimen.abc_action_bar_default_height_material);
    }

    public static void setScreen(Point size) {
        screen = size;
    }
    public static Point getScreen() {
        return screen;
    }

    public static void setLatest(Boolean check) {
        latest = check;
    }
    public static Boolean isLatestVersion(){
        return latest;
    }

    void setBars(List<BarCard> bars) {
        barSearch = bars;
    }

    public List<BarCard> getBars() {
        return barSearch;
    }

    /** Verifica si el dispositivo cuenta con conexion a intenet */
    public static boolean verificaConexion(Context ctx) {
        boolean bConectado = false;
        ConnectivityManager connec = (ConnectivityManager) ctx
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        // No sÃ³lo wifi, tambiÃ©n GPRS
        NetworkInfo[] redes = connec.getAllNetworkInfo();
        // este bucle deberÃ­a no ser tan Ã±apa
        for (int i = 0; i < 2; i++) {
            // Â¿Tenemos conexiÃ³n? ponemos a true
            if (redes[i].getState() == NetworkInfo.State.CONNECTED) {
                bConectado = true;
            }
        }
        return bConectado;
    }

}
