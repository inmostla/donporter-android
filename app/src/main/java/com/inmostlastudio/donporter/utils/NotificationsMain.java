package com.inmostlastudio.donporter.utils;

import android.util.Log;

import com.parse.Parse;
import com.parse.ParseACL;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParsePush;
import com.parse.ParseUser;
import com.parse.SaveCallback;

/**
 * Created by IN MOSTLA Studio on 10/11/2015.
 *
 * Application class.
 * Se establece para iniciallizarse en Parse para la recepción de notificaciones.
 */


public class NotificationsMain extends android.app.Application {

    @Override
    public void onCreate() {
        super.onCreate();

        Parse.setLogLevel(Parse.LOG_LEVEL_VERBOSE);
        // Initialize the Parse SDK.
        Parse.initialize(this, "lS0QocZQjNi8AKmKYsdRGdKeDS0BZLwLFPJOz5cx", "hJP6ka8Sos8jTuTaXMR2zBBakjEosQLxdSObzta8");

        //*Subscribe to the app channel*//
        ParsePush.subscribeInBackground("broadcast", new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    Log.d("com.parse.push", "successfully subscribed to the broadcast channel.");
                } else {
                    Log.e("com.parse.push", "failed to subscribe for push", e);
                }
            }
        });

        // Save the current Installation to Parse.
        ParseInstallation.getCurrentInstallation().saveInBackground();

        ParseACL defaultACL = new ParseACL();
        ParseUser.enableAutomaticUser();
        ParseACL.setDefaultACL(defaultACL, true);

    }
}

