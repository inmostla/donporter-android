package com.inmostlastudio.donporter.barsInfo;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.ArrayList;

import com.inmostlastudio.donporter.R;

/**
 * Created by IN MOSTLA Studio on 15/09/2015.
 *
 * Datos de cada uno de los bares
 */

public class TheBars {

    private static int imageId, logoId;
    private static boolean fav_state, wish_state;
    private static String zone,maps_location;
    private static String barName, longDescription, address, cardView_motto, contact, parseClass;
    private static ArrayList<String> socialNetworks;
    private static String socialFacebookName, socialFacebookPageId, socialTwitter, socialInstagram;

    private static String pref_Fav, pref_Wish;

    /* Establece la clase con los datos del bar */
    public static void setBar(String name) {
        switch (name) {
            case "IN MOSTLA Studio":
                BarINMOSTLA();
                break;
            case "Bar Uno":
                BarUno();
                break;
            case "Bar Dos":
                BarDos();
                break;
            case "Bar Tres":
                BarTres();
                break;
            case "Bar Cuatro":
                BarCuatro();
                break;
            case "Bar Cinco":
                BarCinco();
                break;
        }
    }

    /* Cambia los valores de preferencias de un determinado bar */
    public static void setPrefState(Context context) {
        SharedPreferences preferences = context.getSharedPreferences("FAV_WISH_FILE", Context.MODE_PRIVATE);
        fav_state = preferences.getBoolean(pref_Fav, false);
        wish_state = preferences.getBoolean(pref_Wish, false);
    }

    /* Métodos para return de los elementos */
    public static String getBarName() {
        return barName;
    }
    public static int getImageId() {
        return imageId;
    }
    public static int getLogoId() {
        return logoId;
    }
    public static boolean getFavState() {
        return fav_state;
    }
    public static boolean getWishState() {
        return wish_state;
    }
    public static String getLongDescription() {
        return longDescription;
    }
    public static String getCardView_motto() {
        return cardView_motto;
    }
    public static String getAddress() {
        return address;
    }
    public static String getContact() {
        return contact;
    }
    public static String getMaps_location() {
        return maps_location;
    }
    public static String getZone() {
        return zone;
    }

    public static ArrayList<String> getSocialNetworks( ) {
        return socialNetworks;
    }
    public static String getSocialFacebookPageId() {
        return socialFacebookPageId;
    }
    public static String getSocialFacebookName() {
        return socialFacebookName;
    }
    public static String getSocialTwitter() {
        return socialTwitter;
    }
    public static String getSocialInstagram() {
        return socialInstagram;
    }
    public static String getPref_Fav() {
        return pref_Fav;
    }
    public static String getPref_Wish() {
        return pref_Wish;
    }
    public static boolean getPrefState(Context context, String pref) {
        SharedPreferences preferences = context.getSharedPreferences("FAV_WISH_FILE", Context.MODE_PRIVATE);
        return preferences.getBoolean(pref, false);
    }
    public static String getParseClass() {
        return parseClass;
    }

    /* BARES */
    public static void BarINMOSTLA() {
        barName = "IN MOSTLA Studio"; //barName = ctxt.getString(R.string.app_name);
        parseClass = "Bar_INMOSTLA";
        imageId = R.drawable.bar1;
        logoId = R.drawable.don_porter;
        cardView_motto = "Creando el código del mañana"; //ctxt.getString(R.string.examplebar_motto);
        longDescription = "Empresa dedicada al desarrollo de software," +
                "principalmente aplicaciones móviles y páginas web, dando servicios a empresas de talla local," +
                "hasta un mercado internacional, siendo beneficiadas en mercadotecnia y mejora de servicios" +
                "con la implementación de nuevas tecnologías."; //ctxt.getString(R.string.examplebar_description);
        address = "Acerina #1077, Valle Dorado. \n15:00 - 23:00 hrs"; //ctxt.getString(R.string.examplebar_location);
        contact = "inmostlastudio@gmail.com \nTel: 8308908"; //ctxt.getString(R.string.examplebar_contact);
        maps_location= "22.137234, -100.941408 (IN MOSTLA Studio)";
        zone = "Centro";

        socialNetworks = new ArrayList<>();
        socialNetworks.add("facebook.com/com.inmostlastudio");
        socialNetworks.add("twitter.com/inmostla_studio");
        socialFacebookPageId = "469244299876457";
        socialFacebookName = "inmostlastudio";
        socialTwitter = "donporterapp";
        socialInstagram = "donporterapp";

        pref_Fav = "INMOSTLA_FAV_STATE";
        pref_Wish = "INMOSTLA_WISH_STATE";
    }

    public static void BarUno() {
        barName = "Bar Uno";
        parseClass = "Bar1";
        imageId = R.drawable.bar1;
        logoId = R.drawable.bar_logo1;
        cardView_motto = "Frase del bar";
        longDescription = "Mezcal artesanal, pulque y cerveza, acompañado de auténtica comida " +
                "Mexicana y el mejor ambiente.\n" +
                "Centro Histórico, San Luis Potosí.";
        address = "Acerina #1077, Valle Dorado. San Luis Potosí, SLP.\n" +
                "15:00 -  23:00 hrs";
        contact = "inmostlastudio@gmail.com \nTel: 8226478";
        maps_location= "22.137234, -100.941408 (Bar Uno)";
        zone = "Centro";

        socialNetworks = new ArrayList<>();
        socialNetworks.add("facebook.com/com.inmostlastudio");
        socialFacebookPageId = "469244299876457";
        socialFacebookName = "inmostlastudio";
        socialTwitter = "inmostla_studio";
        socialInstagram = "donporterapp";

        pref_Fav = "BAR1_FAV_STATE";
        pref_Wish = "BAR1_WISH_STATE";
    }

    public static void BarDos() {
        barName = "Bar Dos";
        parseClass = "Bar2";
        imageId = R.drawable.bar2;
        logoId = R.drawable.bar_logo2;
        longDescription = "Mezcal artesanal, pulque y cerveza, acompañado de auténtica comida Mexicana y el mejor ambiente.\n" +
                "Centro Histórico, San Luis Potosí.";
        cardView_motto = "Frase del bar";
        address = "Acerina #1077, Valle Dorado. San Luis Potosí, SLP. \n15:00 -  23:00 hrs";
        contact = "inmostlastudio@gmail.com \nTel: 8308908";
        maps_location= "22.137234, -100.941408 (Bar Dos)";
        zone = "Centro";

        socialNetworks = new ArrayList<>();
        socialNetworks.add("facebook.com/com.inmostlastudio");
        socialFacebookPageId = "469244299876457";
        socialFacebookName = "inmostlastudio";
        socialTwitter = "inmostla_studio";
        socialInstagram = "donporterapp";

        pref_Fav = "BAR2_FAV_STATE";
        pref_Wish = "BAR2_WISH_STATE";
    }

    public static void BarTres() {
        barName = "Bar Tres";
        parseClass = "Bar3";
        imageId = R.drawable.bar3;
        logoId = R.drawable.bar_logo3;
        longDescription = "Mezcal artesanal, pulque y cerveza, acompañado de auténtica comida Mexicana y el mejor ambiente.\n" +
                "Centro Histórico, San Luis Potosí.";
        cardView_motto = "Frase del bar";
        address = "Acerina #1077, Valle Dorado. San Luis Potosí, SLP.\n" +
                "15:00 -  23:00 hrs";
        contact = "inmostlastudio@gmail.com \nTel: 8308908";
        maps_location= "22.137234, -100.941408 (Bar Tres)";
        zone = "Centro";

        socialNetworks = new ArrayList<>();
        socialNetworks.add("facebook.com/com.inmostlastudio");
        socialFacebookPageId = "469244299876457";
        socialFacebookName = "inmostlastudio";
        socialTwitter = "twitter.com/inmostla_studio";
        socialInstagram = "instagram.com/donporterapp";

        pref_Fav = "BAR3_FAV_STATE";
        pref_Wish = "BAR3_WISH_STATE";
    }

    public static void BarCuatro() {
        barName = "Bar Cuatro";
        parseClass = "Bar4";
        imageId = R.drawable.bar4;
        logoId = R.drawable.bar_logo4;
        longDescription = "Mezcal artesanal, pulque y cerveza, acompañado de auténtica comida " +
                "Mexicana y el mejor ambiente.\n" +
                "Centro Histórico, San Luis Potosí.";
        cardView_motto = "Frase del bar";
        address = "Acerina #1077, Valle Dorado. San Luis Potosí, SLP.\n" +
                "15:00 -  23:00 hrs";
        contact = "inmostlastudio@gmail.com \nTel: 8308908";
        maps_location= "22.137234, -100.941408 (Bar Cuatro)";
        zone = "Centro";

        socialNetworks = new ArrayList<>();
        socialNetworks.add("facebook.com/com.inmostlastudio");
        socialFacebookPageId = "469244299876457";
        socialFacebookName = "inmostlastudio";
        socialTwitter = "inmostla_studio";
        socialInstagram = "donporterapp";

        pref_Fav = "BAR4_FAV_STATE";
        pref_Wish = "BAR4_WISH_STATE";
    }

    public static void BarCinco() {
        barName = "Bar Cinco";
        parseClass = "Bar5";
        imageId = R.drawable.bar5;
        logoId = R.drawable.bar_logo5;
        longDescription = "Mezcal artesanal, pulque y cerveza, acompañado de auténtica comida " +
                "Mexicana y el mejor ambiente.\n" +
                "Centro Histórico, San Luis Potosí.";
        cardView_motto = "Frase del bar";
        address = "Acerina #1077, Valle Dorado. San Luis Potosí, SLP.\n" +
                "15:00 -  23:00 hrs";
        contact = "inmostlastudio@gmail.com \nTel: 8308908";
        maps_location= "22.137234, -100.941408 (Bar Cinco)";
        zone = "Centro";

        socialNetworks = new ArrayList<>();
        socialNetworks.add("facebook.com/com.inmostlastudio");
        socialNetworks.add("twitter.com/INMOSTLA_studio");
        socialFacebookPageId = "469244299876457";
        socialFacebookName = "inmostlastudio";
        socialTwitter = "inmostla_studio";
        socialInstagram = "donporterapp";

        pref_Fav = "BAR5_FAV_STATE";
        pref_Wish = "BAR5_WISH_STATE";
    }

}
