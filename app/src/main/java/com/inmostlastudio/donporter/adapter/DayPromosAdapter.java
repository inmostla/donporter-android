package com.inmostlastudio.donporter.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.inmostlastudio.donporter.R;
import com.inmostlastudio.donporter.barlistcontents.DayPromosCard;
import com.inmostlastudio.donporter.thebar.promotions.PromosEventsBar;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by IN MOSTLA Studio on 05/01/2016.
 *
 */

public class DayPromosAdapter extends BaseAdapter {

    Context mContext;
    LayoutInflater inflater;
    List<DayPromosCard> promosCardList;

    /* Constructor */
    public DayPromosAdapter(Context context, List<DayPromosCard> list) {
        mContext = context;
        promosCardList = list;
        inflater = LayoutInflater.from(context);
    }

    public class PromosViewHolder {
        ImageView logo;
        TextView name, description;
    }

    @Override
    public int getCount() {
        return promosCardList.size();
    }

    @Override
    public Object getItem(int position) {
        return promosCardList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final PromosViewHolder holder;
        if(convertView == null) {
            holder = new PromosViewHolder();
            inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.daypromos_item, null);

            holder.logo = (ImageView) convertView.findViewById(R.id.daypromos_image);
            holder.name = (TextView) convertView.findViewById(R.id.daypromos_name);
            holder.description = (TextView) convertView.findViewById(R.id.daypromos_description);

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent barPromos = new Intent(mContext, PromosEventsBar.class);
                    barPromos.putExtra("NAME", promosCardList.get(position).getBarName());
                    barPromos.putExtra("EVENTS_PROMOS", "promos");
                    mContext.startActivity(barPromos);
                }
            });
            convertView.setTag(holder);
        } else {
            holder = (PromosViewHolder) convertView.getTag();
        }

        Picasso.with(mContext).load(promosCardList.get(position).getBarLogoUrl()).into(holder.logo);
        holder.name.setText(promosCardList.get(position).getBarName());
        holder.description.setText(promosCardList.get(position).getPromoDescription());

        return convertView;
    }

}
