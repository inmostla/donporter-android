package com.inmostlastudio.donporter.adapter;

import android.graphics.Point;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.inmostlastudio.donporter.barlistcontents.BarSearchCard;

import com.inmostlastudio.donporter.R;


/**
 * Created by IN MOSTLA Studio on 04/09/2015.
 *
 * RecyclerView Holder para el sistema de b~squeda
 */

public class BarsSearchViewHolder extends RecyclerView.ViewHolder {

    Point screen;
    CardView cardView;
    ImageView barLogo;
    TextView barName, barMotto;

    public BarsSearchViewHolder(View itemView, Point size) {
        super(itemView);
        screen = size;

        cardView = (CardView) itemView.findViewById(R.id.cardview_bars_search);
        barLogo = (ImageView) itemView.findViewById(R.id.search_bar_image);
        barName = (TextView) itemView.findViewById(R.id.search_bar_name);
        barMotto = (TextView) itemView.findViewById(R.id.search_bar_motto);

    }

    public void bind(BarSearchCard card) {
        setCardSize();

        barLogo.setImageResource(card.getLogoId());
        barName.setText(card.getBarName());
        barMotto.setText(card.getBarMotto());
    }

    public void setCardSize() {
        if(screen.y < screen.x) {
            cardView.getLayoutParams().width = screen.x - screen.x/4;
        }
    }
}
