package com.inmostlastudio.donporter.adapter;

import android.content.Context;
import android.graphics.Point;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.inmostlastudio.donporter.R;

/**
 * Created by IN MOSTLA Studio on 13/09/2015.
 *
 * ViewHolder for the RecyclerView showing the bar list.
 */

public class BarsRecyclerViewHolder extends RecyclerView.ViewHolder {

    // Variables
    Point size;
    CardView cardView;
    ImageView barImage, barLogo,favButton, wishlistButton;
    TextView barName, barMotto;
    View images, text;

    String fav_pref, wish_pref;
    boolean fav_state, wish_state;

    /* Constructor. Busca los elementos en el layout */
    BarsRecyclerViewHolder(View itemView, Point size){
        super(itemView);
        this.size = size;

        cardView = (CardView) itemView.findViewById(R.id.cv_bars);
        images = itemView.findViewById(R.id.card_images);
        text = itemView.findViewById(R.id.card_texts);

        barImage = (ImageView) itemView.findViewById(R.id.bar_image_fragment);
        barLogo = (ImageView) itemView.findViewById(R.id.bar_logo_cardview_fragment);
        barName = (TextView) itemView.findViewById(R.id.bar_name_fragment);
        barMotto = (TextView) itemView.findViewById(R.id.bar_motto_fragment);
        favButton = (ImageView) itemView.findViewById(R.id.fav_button_fragment);
        wishlistButton = (ImageView) itemView.findViewById(R.id.wish_button_fragment);
    }

    /* Establece el estado de favoritos */
    public void setFavButton(boolean state){
        if(state)
            favButton.setImageResource(R.drawable.ic_fav_star_check);
        else
            favButton.setImageResource(R.drawable.ic_fav_star_uncheck);
    }
    /* Establece el estado de Wishlist */
    public void setWishButton(boolean state){
        if(state)
            wishlistButton.setImageResource(R.drawable.ic_wishlist_check);
        else
            wishlistButton.setImageResource(R.drawable.ic_wishlist_uncheck);
    }

    public void setCardSize(Context context) {
//        int width;
        int height;

        if(size.y > size.x) {
            height = size.y/2 - 2*(int) context.getResources().getDimension(R.dimen.activity_vertical_margin);
//            width = size.x;
        } else {
            height = size.y - 2*(int) context.getResources().getDimension(R.dimen.activity_vertical_margin);
//            width = size.x - size.x/4;
        }

//        cardView.getLayoutParams().width = width;
        images.getLayoutParams().height = height - height/5;

        RelativeLayout.LayoutParams layoutParams =
                new RelativeLayout.LayoutParams(barLogo.getLayoutParams());
        layoutParams.height = height/3;
        layoutParams.setMargins(0,height/6,0,0);
        layoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
        barLogo.setLayoutParams(layoutParams);
        barLogo.setScaleType(ImageView.ScaleType.CENTER_INSIDE);

        favButton.getLayoutParams().height = height/10;
        favButton.getLayoutParams().width = height/10;
        wishlistButton.getLayoutParams().height = height/10;
        wishlistButton.getLayoutParams().width = height/10;
    }

}