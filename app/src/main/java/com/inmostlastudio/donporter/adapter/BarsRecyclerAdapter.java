package com.inmostlastudio.donporter.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.inmostlastudio.donporter.barlistcontents.BarCard;
import com.inmostlastudio.donporter.thebar.BarHome;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import com.inmostlastudio.donporter.R;

/**
 * Created by IN MOSTLA Studio on 18/08/2015.
 *
 * Recycler adapter for the Bars list
 *
 */

// TODO DESIGN: 14/10/2015 Canvas para un menor overdraw
// TODO: 20/01/2016 Crear Snackbar para las acciones

public class BarsRecyclerAdapter extends RecyclerView.Adapter<BarsRecyclerViewHolder> {

    // Variables
    Point size;     // Coordenadas para conocer las medidas de la pantalla
    Context cxt;    // Contexto para conocer de qué actividad se inicia
    SharedPreferences preferences;  // Preferencias para favoritos y wishlist
    SharedPreferences.Editor editor;    // Editor para editar las preferencias
    List<BarCard> bars;     // Lista de bares
    CoordinatorLayout coordinatorLayout;

    /* Constructor. Inicia los valores */
    public BarsRecyclerAdapter(Context context, List<BarCard> bars, Point size, CoordinatorLayout coord) {
        cxt = context;
        this.size = size;
        this.bars = new ArrayList<>(bars);
        coordinatorLayout = coord;
        preferences = cxt.getSharedPreferences("FAV_WISH_FILE", Context.MODE_PRIVATE);
    }

    /* Obtiene la cuenta de los elementos. */
    @Override
    public int getItemCount(){
        return bars.size();
    }

    /* "Infla" el layout correspondiente */
    @Override
    public BarsRecyclerViewHolder onCreateViewHolder(ViewGroup viewGroup, int position) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.cardview_bars, viewGroup, false);
        return new BarsRecyclerViewHolder(v, size);
    }

    /* Asigna los valores a cada elemento */
    @Override
    public void onBindViewHolder(final BarsRecyclerViewHolder barsRecyclerViewHolder,  final int position) {
        // Establece el tamaño de la tarjeta
        barsRecyclerViewHolder.setCardSize(cxt);

        // Picasso para cargar las imágenes de cada bar
        Picasso.with(cxt).load(bars.get(position).getImageId()).into(barsRecyclerViewHolder.barImage);
        Picasso.with(cxt).load(bars.get(position).getLogoId()).into(barsRecyclerViewHolder.barLogo);

        // Nombres del bar
        barsRecyclerViewHolder.barName.setText(bars.get(position).getName());
        barsRecyclerViewHolder.barMotto.setText(bars.get(position).getMotto());

        // Obtiene las preferencias
        barsRecyclerViewHolder.fav_pref = bars.get(position).getPref_fav();
        barsRecyclerViewHolder.fav_state = bars.get(position).getFavState();
        barsRecyclerViewHolder.setFavButton(barsRecyclerViewHolder.fav_state);
        barsRecyclerViewHolder.wish_pref = bars.get(position).getPref_wish();
        barsRecyclerViewHolder.wish_state = bars.get(position).getWishState();
        barsRecyclerViewHolder.setWishButton(barsRecyclerViewHolder.wish_state);

        // Agrega un clickListener al botón de favoritos
        barsRecyclerViewHolder.favButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (barsRecyclerViewHolder.fav_state) {
                    barsRecyclerViewHolder.fav_state = false;
//                    Snackbar snackbar = Snackbar.make(coordinatorLayout, "Done", Snackbar.LENGTH_LONG);
//                        snackbar.show();
                    Toast.makeText(cxt, "Bar removido de los favoritos", Toast.LENGTH_SHORT).show();
                } else {
                    barsRecyclerViewHolder.fav_state = true;
//                    Snackbar snackbar = Snackbar.make(coordinatorLayout, "Done2", Snackbar.LENGTH_LONG);
//                    snackbar.show();
                    Toast.makeText(cxt, "Bar agregado a los favoritos", Toast.LENGTH_SHORT).show();
                    if (barsRecyclerViewHolder.wish_state) {
                        barsRecyclerViewHolder.wish_state = false;
                    }
                }
                barsRecyclerViewHolder.setFavButton(barsRecyclerViewHolder.fav_state);
                barsRecyclerViewHolder.setWishButton(barsRecyclerViewHolder.wish_state);
                bars.get(position).setFavWish(barsRecyclerViewHolder.fav_state, barsRecyclerViewHolder.wish_state);
                setPreferences(barsRecyclerViewHolder.fav_pref, barsRecyclerViewHolder.wish_pref,
                        barsRecyclerViewHolder.fav_state, barsRecyclerViewHolder.wish_state);
            }
        });
        // Agrega un clickListener al botón de wishList
        barsRecyclerViewHolder.wishlistButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(barsRecyclerViewHolder.wish_state) {
                    barsRecyclerViewHolder.wish_state = false;
                    Toast.makeText(cxt, "Bar removido de la lista de deseos", Toast.LENGTH_SHORT).show();
                }
                else {
                    barsRecyclerViewHolder.wish_state = true;
                    Toast.makeText(cxt, "Bar agregado a la lista de deseos", Toast.LENGTH_SHORT).show();
                    if(barsRecyclerViewHolder.fav_state) {
                        barsRecyclerViewHolder.fav_state = false;
                    }
                }

                barsRecyclerViewHolder.setWishButton(barsRecyclerViewHolder.wish_state);
                barsRecyclerViewHolder.setFavButton(barsRecyclerViewHolder.fav_state);
                bars.get(position).setFavWish(barsRecyclerViewHolder.fav_state, barsRecyclerViewHolder.wish_state);
                setPreferences(barsRecyclerViewHolder.fav_pref, barsRecyclerViewHolder.wish_pref,
                        barsRecyclerViewHolder.fav_state, barsRecyclerViewHolder.wish_state);
            }
        });

        // Con un click normal entra al bar correspondiente que se presionó
        barsRecyclerViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent theBar = new Intent(cxt, BarHome.class);
                theBar.putExtra("NAME", barsRecyclerViewHolder.barName.getText());
                cxt.startActivity(theBar);
            }
        });

        if(position == bars.size()) {
            RelativeLayout.LayoutParams layoutParams =
                    new RelativeLayout.LayoutParams(barsRecyclerViewHolder.cardView.getLayoutParams());
            layoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
            layoutParams.setMargins(0,(int)cxt.getResources().getDimension(R.dimen.activity_vertical_margin),
                    0,size.y/4);
        }
    }

    // Recicla la accion del presionado largo
    @Override
    public void onViewRecycled(BarsRecyclerViewHolder holder) {
        holder.itemView.setOnLongClickListener(null);
        super.onViewRecycled(holder);
    }

    // Establece las opciones al Recycler
    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    // Modifica las preferencias en caso de hacer cklick en un botón
    public void setPreferences(String prefF, String prefW, boolean fav, boolean wish) {
        //Change SharedPreferences state
        editor = preferences.edit();
        editor.putBoolean(prefF, fav);
        editor.putBoolean(prefW, wish);
        editor.apply();
    }

    // Anima el Recycler para filtar los elementos al momento de una selección del FAB
    public void animateTo(List<BarCard> cards) {
        if(!cards.isEmpty()) {
            applyAndAnimateRemovals(cards);
            applyAndAnimateAdditions(cards);
            applyAndAnimateMovedItems(cards);
        }
        else
            bars.clear();
    }

    // Aplica y anima los elementos que se movieron
    private void applyAndAnimateMovedItems(List<BarCard> barCards) {
        for (int toPosition = barCards.size() - 1; toPosition >= 0; toPosition--) {
            final BarCard model = barCards.get(toPosition);
            final int fromPosition = bars.indexOf(model);
            if (fromPosition >= 0 && fromPosition != toPosition) {
                moveItem(fromPosition, toPosition);
            }
        }
    }

    // Aplica la animación a los elementos que se removieron
    private void applyAndAnimateRemovals(List<BarCard> barCards) {
        for (int i = bars.size() - 1; i >= 0; i--) {
            final BarCard model = bars.get(i);
            if (!barCards.contains(model)) {
                removeItem(i);
            }
        }
    }

    // Aplica la animación a los elementos que se agregan
    private void applyAndAnimateAdditions(List<BarCard> barCards) {

        for (int i = 0, count = barCards.size(); i < count; i++) {
            final BarCard model = barCards.get(i);
            if (!bars.contains(model)) {
                addItem(i, model);
            }
        }

    }

    // Remueve un elemento de la lista
    public BarCard removeItem(int position) {
        final BarCard model = bars.remove(position);
        notifyItemRemoved(position);
        return model;
    }

    // Agrega un elemento a la lista
    public void addItem(int position, BarCard model) {
        bars.add(position, model);
        notifyItemInserted(position);
    }

    // Mueve un elemento de la lista
    public void moveItem(int fromPosition, int toPosition) {
        final BarCard model = bars.remove(fromPosition);
        bars.add(toPosition, model);
        notifyItemMoved(fromPosition, toPosition);
    }

}
