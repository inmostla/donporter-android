package com.inmostlastudio.donporter.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import com.inmostlastudio.donporter.R;
import com.inmostlastudio.donporter.thebar.BarHome;
import com.inmostlastudio.donporter.barlistcontents.BarSearchCard;

/**
 * Created by IN MOSTLA Studio on 04/09/2015.
 *
 * RecyclerView Adapter para el sistema de b~squeda
 */

public class BarsSearchAdapter extends RecyclerView.Adapter<BarsSearchViewHolder> {

    //Variables
    private final LayoutInflater inflater;
    private final List<BarSearchCard> mBars;

    Context context;
    Point screen;

    /* Constructor */
    public BarsSearchAdapter(Context context, List<BarSearchCard> bars, Point size) {
        this.context = context;
        screen = size;
        inflater = LayoutInflater.from(context);
        mBars = new ArrayList<>(bars);
    }

    /* Regresa el tamaño de la lista */
    @Override
    public int getItemCount(){
        return mBars.size();
    }

    /* Crea el ViewHolder para la vista del layout*/
    @Override
    public BarsSearchViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        final View itemView = inflater.inflate(R.layout.cardview_search, viewGroup, false);
        return new BarsSearchViewHolder(itemView, screen);
    }

    /* Establece los datos de cada elemento */
    @Override
    public void onBindViewHolder(final BarsSearchViewHolder barsViewHolder,  int position) {
        final BarSearchCard card = mBars.get(position);
        barsViewHolder.bind(card);

        //ClickListener para cada elemento e iniciar la actividad
        barsViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent theBar = new Intent(context, BarHome.class);
                theBar.putExtra("NAME", barsViewHolder.barName.getText());
                context.startActivity(theBar);
            }
        });
    }

    /* Anima la lista de elementos */
    public void animateTo(List<BarSearchCard> cards) {
        applyAndAnimateRemovals(cards);
        applyAndAnimateAdditions(cards);
        applyAndAnimateMovedItems(cards);
    }

    /* Anima los elementos que se movieron */
    private void applyAndAnimateMovedItems(List<BarSearchCard> barCards) {
        for (int toPosition = barCards.size() - 1; toPosition >= 0; toPosition--) {
            final BarSearchCard model = barCards.get(toPosition);
            final int fromPosition = mBars.indexOf(model);
            if (fromPosition >= 0 && fromPosition != toPosition) {
                moveItem(fromPosition, toPosition);
            }
        }
    }

    /* Anima los elemantos removidos */
    private void applyAndAnimateRemovals(List<BarSearchCard> barCards) {
        for (int i = mBars.size() - 1; i >= 0; i--) {
            final BarSearchCard model = mBars.get(i);
            if (!barCards.contains(model)) {
                removeItem(i);
            }
        }
    }

    /* Anima los elementos agregados */
    private void applyAndAnimateAdditions(List<BarSearchCard> barCards) {
        for (int i = 0, count = barCards.size(); i < count; i++) {
            final BarSearchCard model = barCards.get(i);
            if (!mBars.contains(model)) {
                addItem(i, model);
            }
        }
    }

    /* Remueve un elemento */
    public BarSearchCard removeItem(int position) {
        final BarSearchCard model = mBars.remove(position);
        notifyItemRemoved(position);
        return model;
    }

    /* Agrega un elemento */
    public void addItem(int position, BarSearchCard model) {
        mBars.add(position, model);
        notifyItemInserted(position);
    }

    /* Mueve un elemento */
    public void moveItem(int fromPosition, int toPosition) {
        final BarSearchCard model = mBars.remove(fromPosition);
        mBars.add(toPosition, model);
        notifyItemMoved(fromPosition, toPosition);
    }

}
